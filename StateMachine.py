# StateMachine/FiniteStateMachine.py

import random
import threading
import time
from threading import Event, Thread
# Formerly: from threading import Event, Condition, Barrier, Thread, current_thread
# Note: Especially with Condition (replacing Events) we could still experiment, or Barrier (as a multi-thread sync-point)

from DataStorage import DataStorage

# Collect "report items" in this list being used for the Data Logger (class DataStorage)
trace_items = []

# All currently defined events
eActivateDevice = Event()
eDeactivateDevice = Event()
ePerformProvisioning = Event()
eUpdateAlertPolicy = Event()
eReconfigWss = Event()
eReactOnError = Event()

# IN-data lists (inData = old style + one per badge)
inData = []
tempData = []
inData_badge1 = []
inData_badge2 = []
inData_badge3 = []
inData_badge4 = []

# Time interval for generating events
eventGenerationTime = 15

# CLASS FsmThread [idea: start and stop these threads]


class FsmThread(threading.Thread):
    def __init__(self, func):
        self.running = False
        self.function = func
        super(FsmThread, self).__init__()

    def start(self):
        self.running = True
        super(FsmThread, self).start()

    def run(self):
        while self.running:
            self.function()

    def stop(self):
        self.running = False


# Event Listener Functions (one per event)

def eventListener__eActivateDevice():
    while True:
        print("Wait for event eActivateDevice ...")
        # --- Wait until this event comes true
        eActivateDevice.wait()
        # ToDo: React triggering the "Activate Device" action(s) via HTTPS Restful API
        # --- Reset the event flag and wait for next occurrence
        print("Clear event eActivateDevice.")
        eActivateDevice.clear()


def eventListener__eDeactivateDevice():
    while True:
        print("Wait for event eDeactivateDevice ...")
        # --- Wait until this event comes true
        eDeactivateDevice.wait()
        # ToDo: React triggering the "Deactivate Device" action(s) via HTTPS Restful API
        # --- Reset the event flag
        print("Clear event eDeactivateDevice.")
        eDeactivateDevice.clear()


def eventListener__ePerformProvisioning():
    while True:
        print("Wait for event ePerformProvisioning ...")
        # --- Wait until this event comes true
        ePerformProvisioning.wait()
        # ToDo: React triggering the "Perform Provisioning" action(s) via HTTPS Restful API
        # --- Reset the event flag
        print("Clear event ePerformProvisioning.")
        ePerformProvisioning.clear()


def eventListener__eUpdateAlertPolicy():
    while True:
        print("Wait for event eUpdateAlertPolicy ...")
        # --- Wait until this event comes true
        eUpdateAlertPolicy.wait()
        # ToDo: React triggering the "Update Alert Policy" action(s) via HTTPS Restful API
        # --- Reset the event flag
        print("Clear event eUpdateAlertPolicy.")
        eUpdateAlertPolicy.clear()


def eventListener__eReconfigWss():
    while True:
        print("Wait for event eReconfigWss ...")
        # --- Wait until this event comes true
        eReconfigWss.wait()
        # ToDo: React triggering the "Reconfigure the WSS interface" action(s) via HTTPS Restful API
        # --- Reset the event flag
        print("Clear event eReconfigWss.")
        eReconfigWss.clear()


def eventListener__eReactOnError():
    while True:
        print("Wait for event eReactOnError ...")
        # --- Wait until this event comes true
        eReactOnError.wait()
        # ToDo: React triggering the "React on Error" action(s) via HTTPS Restful API
        # Note: This is a general "placeholder" for a number of specific error events!
        # --- Reset the event flag
        print("Clear event eReactOnError.")
        eReactOnError.clear()


# Emulation of WSS Stream and HTTPS RESTful interface

def WSSandHTTPSactor():
    print(">>> Start WSSandHTTPSactor -- start and stop threads, each one per event")
    # Below: Representing "core" of the system part outside FSM
    print("++++++ Running EVENT LISTENERs, each one as per a proprietary FsmThread")
    ft1 = FsmThread(func=eventListener__eActivateDevice)
    ft1.start()
    ft2 = FsmThread(func=eventListener__eDeactivateDevice)
    ft2.start()
    ft3 = FsmThread(func=eventListener__ePerformProvisioning)
    ft3.start()
    ft4 = FsmThread(func=eventListener__eUpdateAlertPolicy)
    ft4.start()
    ft5 = FsmThread(func=eventListener__eReconfigWss)
    ft5.start()
    ft6 = FsmThread(func=eventListener__eReactOnError)
    ft6.start()
    time.sleep(2)
    ft1.stop()
    ft2.stop()
    ft3.stop()
    ft4.stop()
    ft5.stop()
    ft6.stop()
    print(">>>>>> Terminate each of the Listener FsmThreads after a defined time")
    print()
    #   (3) Write to the HTTPS Restful API triggering the requested action(s) -- ToDo (if possible to implement)


# Generate events to trigger actions via the HTTPS RESTful interface

def eventGenerator(data):
    time.sleep(random.randrange(1, 3))  # Sleep 1, 2 or 3 secs.
    # Create a random number between 1 and 6
    randNum = random.randrange(1, 6)
    if randNum == 1:
        # --- Set event eActivateDevice
        print("Set event eActivateDevice.")
        eActivateDevice.set()
    if randNum == 2:
        # --- Set event eDeactivateDevice
        print("Set event eDeactivateDevice.")
        eDeactivateDevice.set()
    if randNum == 3:
        # --- Set event ePerformProvisioning
        print("Set event ePerformProvisioning.")
        ePerformProvisioning.set()
    if randNum == 4:
        # --- Set event eUpdateAlertPolicy
        print("Set event eUpdateAlertPolicy.")
        eUpdateAlertPolicy.set()
    if randNum == 5:
        # --- Set event eReconfigWss
        print("Set event eReconfigWss.")
        eReconfigWss.set()
    if randNum == 6:
        # --- Set event eReactOnError
        print("Set event eReactOnError.")
        eReactOnError.set()
    # Exit criteria
    if data == 'end':
        return


# Idea of the HasBadge class: may be used, after rework, to allign badge with a registered user

class HasBadge:
    def __init__(self, name):
        self.name = name

    def retName(self):
        return self.name


# Transition Table -- initial idea (dict style):
"""
 {(CurrentState) : (ConditionA, ActionA, NextState),
  (CurrentState) : (ConditionB, ActionB, NextState),
  (CurrentState) : (ConditionC, ActionC, NextState),
  ...
 }
"""


# Emulation of WebSocket IN-Stream data --> create a single inData list -- no more used:

def createInStream():
    print(">>> Create IN-Stream")
    # Open file and read line without \n
    fd = open('testPattern.txt', 'r')
    lines = fd.read().splitlines()
    # Collect each single line as a set element and print it out
    for i in lines:
        inData.append(i)
    # [DEBUG] Print-out the list
    print(inData)


# Emulation of WebSocket IN-Stream data --> create inData lists, one per badge -- used:

def createInStreamLists():
    print(">>> Create IN-Stream lists")
    # Open file and read line without \n
    fd = open('testPattern.txt', 'r')
    lines = fd.read().splitlines()
    # Collect each single line as a set element and print it out
    for i in lines:
        tempData.append(i)
        # Represents --> self.state == 'EnterRoom'
        if i == 'Proximity-Badge1-RoomEnter_S':
            inData_badge1.append(tempData[0])
            del tempData[tempData.index('Enter-Room')]
            inData_badge1.append(tempData[0])
            del tempData[tempData.index('Motion-RoomDoor')]
            inData_badge1.append(i)
            del tempData[tempData.index('Proximity-Badge1-RoomEnter_S')]
        elif i == 'Proximity-Badge2-RoomEnter_S':
            inData_badge2.append(tempData[0])
            del tempData[tempData.index('Enter-Room')]
            inData_badge2.append(tempData[0])
            del tempData[tempData.index('Motion-RoomDoor')]
            inData_badge2.append(i)
            del tempData[tempData.index('Proximity-Badge2-RoomEnter_S')]
        elif i == 'Proximity-Badge3-RoomEnter_S':
            inData_badge3.append(tempData[0])
            del tempData[tempData.index('Enter-Room')]
            inData_badge3.append(tempData[0])
            del tempData[tempData.index('Motion-RoomDoor')]
            inData_badge3.append(i)
            del tempData[tempData.index('Proximity-Badge3-RoomEnter_S')]
        elif i == 'Proximity-Badge4-RoomEnter_S':
            inData_badge4.append(tempData[0])
            del tempData[tempData.index('Enter-Room')]
            inData_badge4.append(tempData[0])
            del tempData[tempData.index('Motion-RoomDoor')]
            inData_badge4.append(i)
            del tempData[tempData.index('Proximity-Badge4-RoomEnter_S')]
        else:
            pass
        # Represents --> self.state == 'ApproachDispenser'
        if i == 'Proximity-Badge1-AtDispenser':
            inData_badge1.append(tempData[0])
            del tempData[tempData.index('Motion-Dispenser')]
            inData_badge1.append(tempData[0])
            del tempData[tempData.index('Motion-Badge1-AtDispenser')]
            inData_badge1.append(i)
            del tempData[tempData.index('Proximity-Badge1-AtDispenser')]
        elif i == 'Proximity-Badge2-AtDispenser':
            inData_badge2.append(tempData[0])
            del tempData[tempData.index('Motion-Dispenser')]
            inData_badge2.append(tempData[0])
            del tempData[tempData.index('Motion-Badge2-AtDispenser')]
            inData_badge2.append(i)
            del tempData[tempData.index('Proximity-Badge2-AtDispenser')]
        elif i == 'Proximity-Badge3-AtDispenser':
            inData_badge3.append(tempData[0])
            del tempData[tempData.index('Motion-Dispenser')]
            inData_badge3.append(tempData[0])
            del tempData[tempData.index('Motion-Badge3-AtDispenser')]
            inData_badge3.append(i)
            del tempData[tempData.index('Proximity-Badge3-AtDispenser')]
        elif i == 'Proximity-Badge4-AtDispenser':
            inData_badge4.append(tempData[0])
            del tempData[tempData.index('Motion-Dispenser')]
            inData_badge4.append(tempData[0])
            del tempData[tempData.index('Motion-Badge4-AtDispenser')]
            inData_badge4.append(i)
            del tempData[tempData.index('Proximity-Badge4-AtDispenser')]
        else:
            pass
        # Represents --> self.state == 'ApproachBed1'
        if i == 'Proximity-Badge1-AtBed1':
            if tempData.__contains__('Motion-Badge1-AtBed1'):
                inData_badge1.append(tempData[0])
                del tempData[tempData.index('Motion-Badge1-AtBed1')]
            inData_badge1.append(i)
            del tempData[tempData.index('Proximity-Badge1-AtBed1')]
        elif i == 'Proximity-Badge2-AtBed1':
            if tempData.__contains__('Motion-Badge2-AtBed1'):
                inData_badge2.append(tempData[0])
                del tempData[tempData.index('Motion-Badge2-AtBed1')]
            inData_badge2.append(i)
            del tempData[tempData.index('Proximity-Badge2-AtBed1')]
        elif i == 'Proximity-Badge3-AtBed1':
            if tempData.__contains__('Motion-Badge3-AtBed1'):
                inData_badge3.append(tempData[0])
                del tempData[tempData.index('Motion-Badge3-AtBed1')]
            inData_badge3.append(i)
            del tempData[tempData.index('Proximity-Badge3-AtBed1')]
        elif i == 'Proximity-Badge4-AtBed1':
            if tempData.__contains__('Motion-Badge4-AtBed1'):
                inData_badge4.append(tempData[0])
                del tempData[tempData.index('Motion-Badge4-AtBed1')]
            inData_badge4.append(i)
            del tempData[tempData.index('Proximity-Badge4-AtBed1')]
        else:
            pass
        # Represents --> self.state == 'ApproachBed2'
        if i == 'Proximity-Badge1-AtBed2':
            if tempData.__contains__('Motion-Badge1-AtBed2'):
                inData_badge1.append(tempData[0])
                del tempData[tempData.index('Motion-Badge1-AtBed2')]
            inData_badge1.append(i)
            del tempData[tempData.index('Proximity-Badge1-AtBed2')]
        elif i == 'Proximity-Badge2-AtBed2':
            if tempData.__contains__('Motion-Badge2-AtBed2'):
                inData_badge2.append(tempData[0])
                del tempData[tempData.index('Motion-Badge2-AtBed2')]
            inData_badge2.append(i)
            del tempData[tempData.index('Proximity-Badge2-AtBed2')]
        elif i == 'Proximity-Badge3-AtBed2':
            if tempData.__contains__('Motion-Badge3-AtBed2'):
                inData_badge3.append(tempData[0])
                del tempData[tempData.index('Motion-Badge3-AtBed2')]
            inData_badge3.append(i)
            del tempData[tempData.index('Proximity-Badge3-AtBed2')]
        elif i == 'Proximity-Badge4-AtBed2':
            if tempData.__contains__('Motion-Badge4-AtBed2'):
                inData_badge4.append(tempData[0])
                del tempData[tempData.index('Motion-Badge4-AtBed2')]
            inData_badge4.append(i)
            del tempData[tempData.index('Proximity-Badge4-AtBed2')]
        else:
            pass
        # Represents --> self.state == 'EnterToilet'
        if i == 'Proximity-Badge1-ToiletEnter_S':
            inData_badge1.append(tempData[0])
            del tempData[tempData.index('Motion-ToiletDoor')]
            inData_badge1.append(i)
            del tempData[tempData.index('Proximity-Badge1-ToiletEnter_S')]
        elif i == 'Proximity-Badge2-ToiletEnter_S':
            inData_badge2.append(tempData[0])
            del tempData[tempData.index('Motion-ToiletDoor')]
            inData_badge2.append(i)
            del tempData[tempData.index('Proximity-Badge2-ToiletEnter_S')]
        elif i == 'Proximity-Badge3-ToiletEnter_S':
            inData_badge3.append(tempData[0])
            del tempData[tempData.index('Motion-ToiletDoor')]
            inData_badge3.append(i)
            del tempData[tempData.index('Proximity-Badge3-ToiletEnter_S')]
        elif i == 'Proximity-Badge4-ToiletEnter_S':
            inData_badge4.append(tempData[0])
            del tempData[tempData.index('Motion-ToiletDoor')]
            inData_badge4.append(i)
            del tempData[tempData.index('Proximity-Badge4-ToiletEnter_S')]
        else:
            pass
        # Represents --> self.state == 'ExitToilet'
        if i == 'Proximity-Badge1-ToiletEnter_C':
            inData_badge1.append(tempData[0])
            del tempData[tempData.index('Motion-ToiletDoor')]
            inData_badge1.append(tempData[0])
            del tempData[tempData.index('Motion-Badge1-AtToiletDoor')]
            inData_badge1.append(i)
            del tempData[tempData.index('Proximity-Badge1-ToiletEnter_C')]
        elif i == 'Proximity-Badge2-ToiletEnter_C':
            inData_badge2.append(tempData[0])
            del tempData[tempData.index('Motion-ToiletDoor')]
            inData_badge2.append(tempData[0])
            del tempData[tempData.index('Motion-Badge2-AtToiletDoor')]
            inData_badge2.append(i)
            del tempData[tempData.index('Proximity-Badge2-ToiletEnter_C')]
        elif i == 'Proximity-Badge3-ToiletEnter_C':
            inData_badge3.append(tempData[0])
            del tempData[tempData.index('Motion-ToiletDoor')]
            inData_badge3.append(tempData[0])
            del tempData[tempData.index('Motion-Badge3-AtToiletDoor')]
            inData_badge3.append(i)
            del tempData[tempData.index('Proximity-Badge3-ToiletEnter_C')]
        elif i == 'Proximity-Badge4-ToiletEnter_C':
            inData_badge4.append(tempData[0])
            del tempData[tempData.index('Motion-ToiletDoor')]
            inData_badge4.append(tempData[0])
            del tempData[tempData.index('Motion-Badge4-AtToiletDoor')]
            inData_badge4.append(i)
            del tempData[tempData.index('Proximity-Badge4-ToiletEnter_C')]
        else:
            pass
        # Represents --> self.state == 'ExitRoom'
        if i == 'Proximity-Badge1-RoomEnter_C':
            inData_badge1.append(tempData[0])
            del tempData[tempData.index('Motion-RoomDoor')]
            inData_badge1.append(tempData[0])
            del tempData[tempData.index('Motion-Badge1-AtRoomDoor')]
            inData_badge1.append(tempData[0])
            del tempData[tempData.index('Exit-Room')]
            inData_badge1.append(i)
            del tempData[tempData.index('Proximity-Badge1-RoomEnter_C')]
            inData_badge1.append('Exit')
        elif i == 'Proximity-Badge2-RoomEnter_C':
            inData_badge2.append(tempData[0])
            del tempData[tempData.index('Motion-RoomDoor')]
            inData_badge2.append(tempData[0])
            del tempData[tempData.index('Motion-Badge2-AtRoomDoor')]
            inData_badge2.append(tempData[0])
            del tempData[tempData.index('Exit-Room')]
            inData_badge2.append(i)
            del tempData[tempData.index('Proximity-Badge2-RoomEnter_C')]
            inData_badge2.append('Exit')
        elif i == 'Proximity-Badge3-RoomEnter_C':
            inData_badge3.append(tempData[0])
            del tempData[tempData.index('Motion-RoomDoor')]
            inData_badge3.append(tempData[0])
            del tempData[tempData.index('Motion-Badge3-AtRoomDoor')]
            inData_badge3.append(tempData[0])
            del tempData[tempData.index('Exit-Room')]
            inData_badge3.append(i)
            del tempData[tempData.index('Proximity-Badge3-RoomEnter_C')]
            inData_badge3.append('Exit')
        elif i == 'Proximity-Badge4-RoomEnter_C':
            inData_badge4.append(tempData[0])
            del tempData[tempData.index('Motion-RoomDoor')]
            inData_badge4.append(tempData[0])
            del tempData[tempData.index('Motion-Badge4-AtRoomDoor')]
            inData_badge4.append(tempData[0])
            del tempData[tempData.index('Exit-Room')]
            inData_badge4.append(i)
            del tempData[tempData.index('Proximity-Badge4-RoomEnter_C')]
            inData_badge4.append('Exit')
        else:
            pass
    # Print-out all badge lists
    print(">>> inData_badge1:")
    print(inData_badge1)
    print(">>> inData_badge2:")
    print(inData_badge2)
    print(">>> inData_badge3:")
    print(inData_badge3)
    print(">>> inData_badge4:")
    print(inData_badge4)
    # tempData clean-up
    print(">>> tempData:")
    print(tempData)


# CLASS StateMachine

class StateMachine:
    # Global class members -- not needed

    def __init__(self, name=None):
        if name is None:
            self.name = ""
            print(">>> Nameless FSM instance")
        else:
            self.name = name
            print(">>>>>> Name of FSM instance: " + str(self.name))
        self.state = 'Start'
        self.oldstate = 'Start'
        # Note: eventGeneration is moved out of this class now and also kicked-off separately

    # Condition checks

    # Note: for a single condition check it is not worth changing to "table processing" - so let as is.
    def checkRoomEnterConds(self):
        self.oldstate = 'Start'
        while True:
            if self.to_room_check():
                break

    def checkStayInsideRoomConds(self):
        # Transition Table for state 'EnterRoom'
        cond_list = [self.to_dispenser_check(),
                     self.to_toilet_check(),
                     self.to_room_exit_check(),
                     self.to_bed1_check(),
                     self.to_bed2_check()]
        self.oldstate = 'EnterRoom'
        for func in cond_list:
            if func:
                cond_index = cond_list.index(func)
                # [Debug] print (cond_index)
                if cond_index == 3:
                    self.condPermissionCheck()
                if cond_index == 4:
                    self.condPermissionCheck()
                break

    def checkPostDispenserConds(self):
        self.oldstate = 'ApproachDispenser'
        while True:
            # All OK transitions:
            if self.to_toilet_check():
                self.condPermissionCheck()
                break
            if self.to_room_exit_check():
                self.condPermissionCheck()
                break
            if self.to_bed1_check():
                self.condPermissionCheck()
                break
            if self.to_bed2_check():
                self.condPermissionCheck()
                break

    def checkPostDispenserConds__new(self):
        # Transition Table for state 'ApproachDispenser'
        cond_list = [self.to_toilet_check(),
                     self.to_room_exit_check(),
                     self.to_bed1_check(),
                     self.to_bed2_check()]
        self.oldstate = 'ApproachDispenser'
        # Action Table for state 'ApproachDispenser' -- not needed, not created!
        for func in cond_list:
            # [Debug] print (func)
            if func:
                self.condPermissionCheck()
            break

    def checkPostDispenserCondsBed1(self):
        # Transition Table for state 'ApproachDispenser'
        cond_list = [self.to_toilet_check(),
                     self.to_room_exit_check(),
                     self.to_bed1_check()]
        self.oldstate = 'ApproachDispenser'
        # Action Table for state 'ApproachDispenser' -- not needed, not created!
        for func in cond_list:
            # [Debug] print (func)
            if func:
                self.condPermissionCheck()
                return True

    def checkPostDispenserCondsBed2(self):
        # Transition Table for state 'ApproachDispenser'
        cond_list = [self.to_toilet_check(),
                     self.to_room_exit_check(),
                     self.to_bed2_check()]
        self.oldstate = 'ApproachDispenser'
        # Action Table for state 'ApproachDispenser' -- not needed, not created!
        for func in cond_list:
            # [Debug] print (func)
            if func:
                self.condPermissionCheck()
                return True

    def checkPostPatientBedConds(self):
        self.oldstate = 'ApproachDispenser'
        while True:
            # OK transitions "atomic" checks:
            if self.to_dispenser_check():
                self.condPermissionCheck()
                break
            if self.to_toilet_check():
                self.condPermissionCheck()
                break
            if self.to_room_exit_check():
                self.condPermissionCheck()
                break
            # KO transitions "atomic" checks:
            if self.to_bed1_check():
                # Additional ACTION (below) prior to break
                self.actOnIntraBedMove()
                break
            if self.to_bed2_check():
                # Additional ACTION (below) prior to break
                self.actOnIntraBedMove()
                break

    def checkPostPatientBedConds__new(self):
        # Transition Table for state 'ApproachBed1' & 'ApproachBed2'
        cond_list = [self.to_dispenser_check(),
                     self.to_toilet_check(),
                     self.to_room_exit_check(),
                     self.to_bed1_check(),
                     self.to_bed2_check()]
        # Action Table for state 'ApproachBed1' & 'ApproachBed2'
        act_list = ['None',
                    'None',
                    'None',
                    self.actOnIntraBedMove(),
                    'None']
        for func1, func2 in zip(cond_list, act_list):
            # [Debug] print (func1)
            # [Debug] print (func2)
            if func1:
                cond_index = cond_list.index(func1)
                if act_list[cond_index] != 'None':
                    func2
                else:
                    pass
                break

    def checkPostPatientBed1Conds(self):
        # Transition Table for state 'ApproachBed1'
        cond_list = [self.to_dispenser_check(),
                     self.to_toilet_check(),
                     self.to_room_exit_check(),
                     self.to_bed2_check()]
        for func in cond_list:
            if func:
                cond_index = cond_list.index(func)
                if cond_index == 3:
                    self.condPermissionCheck()
                break

    def checkPostPatientBed2Conds(self):
        # Transition Table for state 'ApproachBed2'
        cond_list = [self.to_dispenser_check(),
                     self.to_toilet_check(),
                     self.to_room_exit_check(),
                     self.to_bed1_check()]
        for func in cond_list:
            if func:
                cond_index = cond_list.index(func)
                if cond_index == 3:
                    self.condPermissionCheck()
                break

    # Note: for a single condition check it is not worth changing to "table processing" - so let as is.
    def checkStayInsideToiletConds(self):
        self.oldstate = 'EnterToilet'
        while True:
            if self.to_toilet_exit_check():
                self.condPermissionCheck()
                break

    def checkPostToiletConds(self):
        # Transition Table for state 'ExitToilet'
        cond_list = [self.to_dispenser_check(),
                     self.to_room_exit_check(),
                     self.to_bed1_check(),
                     self.to_bed2_check()]
        self.oldstate = 'ExitToilet'
        for func in cond_list:
            if func:
                cond_index = cond_list.index(func)
                if cond_index == 2:
                    self.condPermissionCheck()
                if cond_index == 3:
                    self.condPermissionCheck()
                break

    # "FSM-atomic" checks (encapsulation one single state transition, universally used for condition check / state - see above)

    def to_room_check(self):
        condition_1 = [False, False, False]
        for i in inData:
            if i == 'Enter-Room':
                condition_1[0] = True
            elif i == 'Motion-RoomDoor':
                condition_1[1] = True
            elif i == 'Proximity-Badge1-RoomEnter_S' or i == 'Proximity-Badge2-RoomEnter_S' or i == 'Proximity-Badge3-RoomEnter_S' or i == 'Proximity-Badge4-RoomEnter_S':
                condition_1[2] = True
            elif condition_1 == [True, True, True]:
                # --> Clean-up as af eliminate the used list item and trace out alerts
                trace_items.append("SYSTEM ALERTS:")
                if inData.__contains__('Enter-Room'):
                    trace_items.append("   " + str(inData[inData.index('Enter-Room')]))
                    del inData[inData.index('Enter-Room')]
                if inData.__contains__('Motion-RoomDoor'):
                    trace_items.append("   " + str(inData[inData.index('Motion-RoomDoor')]))
                    del inData[inData.index('Motion-RoomDoor')]
                if inData.__contains__('Proximity-Badge1-RoomEnter_S'):
                    trace_items.append("   " + str(inData[inData.index('Proximity-Badge1-RoomEnter_S')]))
                    del inData[inData.index('Proximity-Badge1-RoomEnter_S')]
                if inData.__contains__('Proximity-Badge2-RoomEnter_S'):
                    trace_items.append("   " + str(inData[inData.index('Proximity-Badge2-RoomEnter_S')]))
                    del inData[inData.index('Proximity-Badge2-RoomEnter_S')]
                if inData.__contains__('Proximity-Badge3-RoomEnter_S'):
                    trace_items.append("   " + str(inData[inData.index('Proximity-Badge3-RoomEnter_S')]))
                    del inData[inData.index('Proximity-Badge3-RoomEnter_S')]
                if inData.__contains__('Proximity-Badge4-RoomEnter_S'):
                    trace_items.append("   " + str(inData[inData.index('Proximity-Badge4-RoomEnter_S')]))
                    del inData[inData.index('Proximity-Badge4-RoomEnter_S')]
                # Change state
                self.state = 'EnterRoom'
                return True
            else:
                return False

    def to_dispenser_check(self):
        # Transition OK path #1 for Transition Table exec
        condition_2 = [False, False, False]
        for i in inData:
            if i == 'Motion-Dispenser':
                condition_2[0] = True
                # Extra print-out and trace-out out dispenser status
                print("++++++ Dispenser used ++++++")
                trace_items.append("+++ DISPENSER STATUS: identified as used -- OK")
            elif i == 'Motion-Badge1-AtDispenser' or i == 'Motion-Badge2-AtDispenser' or i == 'Motion-Badge3-AtDispenser' or i == 'Motion-Badge4-AtDispenser':
                condition_2[1] = True
            elif i == 'Proximity-Badge1-AtDispenser' or i == 'Proximity-Badge2-AtDispenser' or i == 'Proximity-Badge3-AtDispenser' or i == 'Proximity-Badge4-AtDispenser':
                condition_2[2] = True
            elif condition_2 == [True, True, True]:
                # --> Clean-up as af eliminate the used list item and trace out alerts
                trace_items.append("SYSTEM ALERTS:")
                if inData.__contains__('Motion-Dispenser'):
                    trace_items.append("   " + str(inData[inData.index('Motion-Dispenser')]))
                    del inData[inData.index('Motion-Dispenser')]
                if inData.__contains__('Motion-Badge1-AtDispenser'):
                    trace_items.append("   " + str(inData[inData.index('Motion-Badge1-AtDispenser')]))
                    del inData[inData.index('Motion-Badge1-AtDispenser')]
                if inData.__contains__('Motion-Badge2-AtDispenser'):
                    trace_items.append("   " + str(inData[inData.index('Motion-Badge2-AtDispenser')]))
                    del inData[inData.index('Motion-Badge2-AtDispenser')]
                if inData.__contains__('Motion-Badge3-AtDispenser'):
                    trace_items.append("   " + str(inData[inData.index('Motion-Badge3-AtDispenser')]))
                    del inData[inData.index('Motion-Badge3-AtDispenser')]
                if inData.__contains__('Motion-Badge4-AtDispenser'):
                    trace_items.append("   " + str(inData[inData.index('Motion-Badge4-AtDispenser')]))
                    del inData[inData.index('Motion-Badge4-AtDispenser')]
                if inData[0].__contains__('Proximity-Badge1-AtDispenser'):
                    trace_items.append("   " + str(inData[inData.index('Proximity-Badge1-AtDispenser')]))
                    del inData[inData.index('Proximity-Badge1-AtDispenser')]
                if inData.__contains__('Proximity-Badge2-AtDispenser'):
                    trace_items.append("   " + str(inData[inData.index('Proximity-Badge2-AtDispenser')]))
                    del inData[inData.index('Proximity-Badge2-AtDispenser')]
                if inData.__contains__('Proximity-Badge3-AtDispenser'):
                    trace_items.append("   " + str(inData[inData.index('Proximity-Badge3-AtDispenser')]))
                    del inData[inData.index('Proximity-Badge3-AtDispenser')]
                if inData.__contains__('Proximity-Badge4-AtDispenser'):
                    trace_items.append("   " + str(inData[inData.index('Proximity-Badge4-AtDispenser')]))
                    del inData[inData.index('Proximity-Badge4-AtDispenser')]
                # Change state
                self.state = 'ApproachDispenser'
                return True
            else:
                return False

    def to_toilet_check(self):
        # Transition OK path #2 for Transition Table exec
        condition_4 = [False, False]
        for i in inData:
            if i == 'Motion-ToiletDoor':
                condition_4[0] = True
            elif i == 'Proximity-Badge1-ToiletEnter_S' or i == 'Proximity-Badge2-ToiletEnter_S' or i == 'Proximity-Badge3-ToiletEnter_S' or i == 'Proximity-Badge4-ToiletEnter_S':
                condition_4[1] = True
            elif condition_4 == [True, True]:
                # --> Clean-up as af eliminate the used list item and trace out alerts
                trace_items.append("SYSTEM ALERTS:")
                # --> Action: clean-up as af eliminate the used list item (to not be used over and over)
                if inData.__contains__('Motion-ToiletDoor'):
                    trace_items.append("   " + str(inData[inData.index('Motion-ToiletDoor')]))
                    del inData[inData.index('Motion-ToiletDoor')]
                if inData.__contains__('Proximity-Badge1-ToiletEnter_S'):
                    trace_items.append("   " + str(inData[inData.index('Proximity-Badge1-ToiletEnter_S')]))
                    del inData[inData.index('Proximity-Badge1-ToiletEnter_S')]
                if inData.__contains__('Proximity-Badge2-ToiletEnter_S'):
                    trace_items.append("   " + str(inData[inData.index('Proximity-Badge2-ToiletEnter_S')]))
                    del inData[inData.index('Proximity-Badge2-ToiletEnter_S')]
                if inData.__contains__('Proximity-Badge3-ToiletEnter_S'):
                    trace_items.append("   " + str(inData[inData.index('Proximity-Badge3-ToiletEnter_S')]))
                    del inData[inData.index('Proximity-Badge3-ToiletEnter_S')]
                if inData.__contains__('Proximity-Badge4-ToiletEnter_S'):
                    trace_items.append("   " + str(inData[inData.index('Proximity-Badge4-ToiletEnter_S')]))
                    del inData[inData.index('Proximity-Badge4-ToiletEnter_S')]
                # Change state
                self.state = 'EnterToilet'
                return True
            else:
                return False

    def to_room_exit_check(self):
        # Transition OK path #3 for Transition Table exec
        condition_6 = [False, False, False, False]
        for i in inData:
            if i == 'Motion-RoomDoor':
                condition_6[0] = True
            elif i == 'Motion-Badge1-AtRoomDoor' or i == 'Motion-Badge2-AtRoomDoor' or i == 'Motion-Badge3-AtRoomDoor' or i == 'Motion-Badge4-AtRoomDoor':
                condition_6[1] = True
            elif i == 'Exit-Room':
                condition_6[2] = True
            elif i == 'Proximity-Badge1-RoomEnter_C' or i == 'Proximity-Badge2-RoomEnter_C' or i == 'Proximity-Badge3-RoomEnter_C' or i == 'Proximity-Badge4-RoomEnter_C':
                condition_6[3] = True
            elif condition_6 == [True, True, True, True]:
                # --> Clean-up as af eliminate the used list item and trace out alerts
                trace_items.append("SYSTEM ALERTS:")
                if inData.__contains__('Motion-RoomDoor'):
                    trace_items.append("   " + str(inData[inData.index('Motion-RoomDoor')]))
                    del inData[inData.index('Motion-RoomDoor')]
                if inData.__contains__('Motion-Badge1-AtRoomDoor'):
                    trace_items.append("   " + str(inData[inData.index('Motion-Badge1-AtRoomDoor')]))
                    del inData[inData.index('Motion-Badge1-AtRoomDoor')]
                if inData.__contains__('Motion-Badge2-AtRoomDoor'):
                    trace_items.append("   " + str(inData[inData.index('Motion-Badge2-AtRoomDoor')]))
                    del inData[inData.index('Motion-Badge2-AtRoomDoor')]
                if inData.__contains__('Motion-Badge3-AtRoomDoor'):
                    trace_items.append("   " + str(inData[inData.index('Motion-Badge3-AtRoomDoor')]))
                    del inData[inData.index('Motion-Badge3-AtRoomDoor')]
                if inData.__contains__('Motion_Badge4-AtRoomDoor'):
                    trace_items.append("   " + str(inData[inData.index('Motion-Badge4-AtRoomDoor')]))
                    del inData[inData.index('Motion-Badge4-AtRoomDoor')]
                if inData.__contains__('Exit-Room'):
                    trace_items.append("   " + str(inData[inData.index('Exit-Room')]))
                    del inData[inData.index('Exit-Room')]
                if inData.__contains__('Proximity-Badge1-RoomEnter_C'):
                    trace_items.append("   " + str(inData[inData.index('Proximity-Badge1-RoomEnter_C')]))
                    del inData[inData.index('Proximity-Badge1-RoomEnter_C')]
                if inData.__contains__('Proximity-Badge2-RoomEnter_C'):
                    trace_items.append("   " + str(inData[inData.index('Proximity-Badge2-RoomEnter_C')]))
                    del inData[inData.index('Proximity-Badge2-RoomEnter_C')]
                if inData.__contains__('Proximity-Badge3-RoomEnter_C'):
                    trace_items.append("   " + str(inData[inData.index('Proximity-Badge3-RoomEnter_C')]))
                    del inData[inData.index('Proximity-Badge3-RoomEnter_C')]
                if inData.__contains__('Proximity-Badge4-RoomEnter_C'):
                    trace_items.append("   " + str(inData[inData.index('Proximity-Badge4-RoomEnter_C')]))
                    del inData[inData.index('Proximity-Badge4-RoomEnter_C')]
                # Change state
                self.state = 'ExitRoom'
                return True
            else:
                return False

    def to_bed1_check(self):
        # Transition KO path #1 for Transition Table exec
        condition_3A = False
        for i in inData:
            if i == 'Motion-Badge1-AtBed1' or i == 'Motion-Badge2-AtBed1' or i == 'Motion-Badge3-AtBed1' or i == 'Motion-Badge4-AtBed1':
                # Print-out and trace-out positive patient status
                print("++++++ Patient ONE gets treated ++++++")
                trace_items.append("+++ PATIENT STATUS: Patient ONE gets treated")
                # Perform list clean-up
                if inData.__contains__('Motion-Badge1-AtBed1'):
                    del inData[inData.index('Motion-Badge1-AtBed1')]
                if inData.__contains__('Motion-Badge2-AtBed1'):
                    del inData[inData.index('Motion-Badge2-AtBed1')]
                if inData.__contains__('Motion-Badge3-AtBed1'):
                    del inData[inData.index('Motion-Badge3-AtBed1')]
                if inData.__contains__('Motion-Badge4-AtBed1'):
                    del inData[inData.index('Motion-Badge4-AtBed1')]
            elif i == 'Proximity-Badge1-AtBed1' or i == 'Proximity-Badge2-AtBed1' or i == 'Proximity-Badge3-AtBed1' or i == 'Proximity-Badge4-AtBed1':
                condition_3A = True
            elif condition_3A:
                # --> Clean-up as af eliminate the used list item and trace out alerts
                trace_items.append("SYSTEM ALERTS:")
                if inData.__contains__('Proximity-Badge1-AtBed1'):
                    trace_items.append("   " + str(inData[inData.index('Proximity-Badge1-AtBed1')]))
                    del inData[inData.index('Proximity-Badge1-AtBed1')]
                if inData.__contains__('Proximity-Badge2-AtBed1'):
                    trace_items.append("   " + str(inData[inData.index('Proximity-Badge2-AtBed1')]))
                    del inData[inData.index('Proximity-Badge2-AtBed1')]
                if inData.__contains__('Proximity-Badge3-AtBed1'):
                    trace_items.append("   " + str(inData[inData.index('Proximity-Badge3-AtBed1')]))
                    del inData[inData.index('Proximity-Badge3-AtBed1')]
                if inData.__contains__('Proximity-Badge4-AtBed1'):
                    trace_items.append("   " + str(inData[inData.index('Proximity-Badge4-AtBed1')]))
                    del inData[inData.index('Proximity-Badge4-AtBed1')]
                # Change state
                self.state = 'ApproachBed1'
                return True
            else:
                return False

    def to_bed2_check(self):
        # Transition KO path #2 for Transition Table exec
        condition_3B = False
        for i in inData:
            if i == 'Motion-Badge1-AtBed2' or i == 'Motion-Badge2-AtBed2' or i == 'Motion-Badge3-AtBed2' or i == 'Motion-Badge4-AtBed2':
                # Print-out and trace-out patient status
                print("++++++ Patient TWO gets treated ++++++")
                trace_items.append("+++ PATIENT STATUS: Patient TWO gets treated")
                # Perform list clean-up
                if inData.__contains__('Motion-Badge1-AtBed2'):
                    del inData[inData.index('Motion-Badge1-AtBed2')]
                if inData.__contains__('Motion-Badge2-AtBed2'):
                    del inData[inData.index('Motion-Badge2-AtBed2')]
                if inData.__contains__('Motion-Badge3-AtBed2'):
                    del inData[inData.index('Motion-Badge3-AtBed2')]
                if inData.__contains__('Motion-Badge4-AtBed2'):
                    del inData[inData.index('Motion-Badge4-AtBed2')]
            elif i == 'Proximity-Badge1-AtBed2' or i == 'Proximity-Badge2-AtBed2' or i == 'Proximity-Badge3-AtBed2' or i == 'Proximity-Badge4-AtBed2':
                condition_3B = True
            elif condition_3B:
                # --> Clean-up as af eliminate the used list item and trace out alerts
                trace_items.append("SYSTEM ALERTS:")
                if inData.__contains__('Proximity-Badge1-AtBed2'):
                    trace_items.append("   " + str(inData[inData.index('Proximity-Badge1-AtBed2')]))
                    del inData[inData.index('Proximity-Badge1-AtBed2')]
                if inData.__contains__('Proximity-Badge2-AtBed2'):
                    trace_items.append("   " + str(inData[inData.index('Proximity-Badge2-AtBed2')]))
                    del inData[inData.index('Proximity-Badge2-AtBed2')]
                if inData.__contains__('Proximity-Badge3-AtBed2'):
                    trace_items.append("   " + str(inData[inData.index('Proximity-Badge3-AtBed2')]))
                    del inData[inData.index('Proximity-Badge3-AtBed2')]
                if inData.__contains__('Proximity-Badge4-AtBed2'):
                    trace_items.append("   " + str(inData[inData.index('Proximity-Badge4-AtBed2')]))
                    del inData[inData.index('Proximity-Badge4-AtBed2')]
                # Change state
                self.state = 'ApproachBed2'
                return True
            else:
                return False

    def to_toilet_exit_check(self):
        condition_5 = [False, False, False]
        for i in inData:
            if i == 'Motion-ToiletDoor':
                condition_5[0] = True
            elif i == 'Motion-Badge1-AtToiletDoor' or i == 'Motion-Badge2-AtToiletDoor' or i == 'Motion-Badge3-AtToiletDoor' or i == 'Motion-Badge4-AtToiletDoor':
                condition_5[1] = True
            elif i == 'Proximity-Badge1-ToiletEnter_C' or i == 'Proximity-Badge2-ToiletEnter_C' or i == 'Proximity-Badge3-ToiletEnter_C' or i == 'Proximity-Badge4-ToiletEnter_C':
                condition_5[2] = True
            elif condition_5 == [True, True, True]:
                # --> Clean-up as af eliminate the used list item and trace out alerts
                trace_items.append("SYSTEM ALERTS:")
                if inData.__contains__('Motion-ToiletDoor'):
                    trace_items.append("   " + str(inData[inData.index('Motion-ToiletDoor')]))
                    del inData[inData.index('Motion-ToiletDoor')]
                if inData.__contains__('Motion-Badge1-AtToiletDoor'):
                    trace_items.append("   " + str(inData[inData.index('Motion-Badge1-AtToiletDoor')]))
                    del inData[inData.index('Motion-Badge1-AtToiletDoor')]
                if inData.__contains__('Motion-Badge2-AtToiletDoor'):
                    trace_items.append("   " + str(inData[inData.index('Motion-Badge2-AtToiletDoor')]))
                    del inData[inData.index('Motion-Badge2-AtToiletDoor')]
                if inData.__contains__('Motion-Badge3-AtToiletDoor'):
                    trace_items.append("   " + str(inData[inData.index('Motion-Badge3-AtToiletDoor')]))
                    del inData[inData.index('Motion-Badge3-AtToiletDoor')]
                if inData.__contains__('Motion-Badge4-AtToiletDoor'):
                    trace_items.append("   " + str(inData[inData.index('Motion-Badge4-AtToiletDoor')]))
                    del inData[inData.index('Motion-Badge4-AtToiletDoor')]
                if inData.__contains__('Proximity-Badge1-ToiletEnter_C'):
                    trace_items.append("   " + str(inData[inData.index('Proximity-Badge1-ToiletEnter_C')]))
                    del inData[inData.index('Proximity-Badge1-ToiletEnter_C')]
                if inData.__contains__('Proximity-Badge2-ToiletEnter_C'):
                    trace_items.append("   " + str(inData[inData.index('Proximity-Badge2-ToiletEnter_C')]))
                    del inData[inData.index('Proximity-Badge2-ToiletEnter_C')]
                if inData.__contains__('Proximity-Badge3-ToiletEnter_C'):
                    trace_items.append("   " + str(inData[inData.index('Proximity-Badge3-ToiletEnter_C')]))
                    del inData[inData.index('Proximity-Badge3-ToiletEnter_C')]
                if inData.__contains__('Proximity-Badge4-ToiletEnter_C'):
                    trace_items.append("   " + str(inData[inData.index('Proximity-Badge4-ToiletEnter_C')]))
                    del inData[inData.index('Proximity-Badge4-ToiletEnter_C')]
                # Change state
                self.state = 'ExitToilet'
                return True
            else:
                return False

    # Actions:

    def TriggerRestApiAction(self):
        # ToDo: set / unset an appropriate SIGNAL / EVENT (tbd) for each new state reached (if needed - tbd)
        pass

    @staticmethod
    def actOnDirectEntranceToBedMove():
        print("------ FORBIDDEN: User directly moves from room entrance to a patient bed !!! ------")
        trace_items.append(">>> FORBIDDEN: User directly moves from room entrance to a patient bed !")
        # ToDo: add more, e.g. save in global member variable, write into DB, trigger fred lamp (for active system), etc.

    @staticmethod
    def actOnIntraBedMove():
        print("------ FORBIDDEN: User moves between patient beds !!! ------")
        trace_items.append(">>> FORBIDDEN: User moves between patient beds !")
        # ToDo: add more, e.g. save in global member variable, write into DB, trigger fred lamp (for active system), etc.

    @staticmethod
    def actOnDirectToiletToBedMove():
        print("------ FORBIDDEN: User exits from toilet and goes directly to a patient bed !!! ------")
        trace_items.append(">>> FORBIDDEN: User exits from toilet and goes directly to a patient bed !")
        # ToDo: add more, e.g. save in global member variable, write into DB, trigger fred lamp (for active system), etc.

    def condPermissionCheck(self):
        # [DEBUG] print("oldstate --->>> " + self.oldstate)
        # [DEBUG] print("   state --->>> " + self.state)
        if self.oldstate == 'EnterRoom' and self.state == 'ApproachDispenser':
            print("------ ALLOWED MOVEMENT ------")
            trace_items.append(">>> ALLOWED MOVEMENT.")
        if self.oldstate == 'EnterRoom' and self.state == 'EnterToilet':
            print("------ ALLOWED MOVEMENT ------")
            trace_items.append(">>> ALLOWED MOVEMENT.")
        if self.oldstate == 'EnterRoom' and self.state == 'ExitRoom':
            print("------ ALLOWED MOVEMENT ------")
            trace_items.append(">>> ALLOWED MOVEMENT.")
        if self.oldstate == 'EnterRoom' and self.state == 'ApproachBed1':
            print("------ FORBIDDEN: User directly moves from room entrance to a patient bed ------")
            trace_items.append(">>> FORBIDDEN: User directly moves from room entrance to a patient bed!")
        if self.oldstate == 'EnterRoom' and self.state == 'ApproachBed2':
            print("------ FORBIDDEN: User directly moves from room entrance to a patient bed ------")
            trace_items.append(">>> FORBIDDEN: User directly moves from room entrance to a patient bed!")
        if self.oldstate == 'ApproachDispenser':
            print("------ ALLOWED MOVEMENT ------")
            trace_items.append(">>> ALLOWED MOVEMENT.")
        if self.oldstate == 'ApproachBed1' and self.state == 'ApproachBed2':
            print("------ FORBIDDEN: User moves between patient beds ------")
            trace_items.append(">>> FORBIDDEN: User moves between patient beds!")
        if self.oldstate == 'ApproachBed2' and self.state == 'ApproachBed1':
            print("------ FORBIDDEN: User moves between patient beds ------")
            trace_items.append(">>> FORBIDDEN: User moves between patient beds!")
        if self.oldstate == 'EnterToilet':
            print("------ ALLOWED (and the only possible) MOVEMENT ------")
            trace_items.append(">>> ALLOWED MOVEMENT.")
        if self.oldstate == 'ExitToilet':
            print("------ FORBIDDEN: User exits from toilet and goes directly to a patient bed ------")
            trace_items.append(">>> FORBIDDEN: User exits from toilet and goes directly to a patient bed!")
        if self.oldstate == 'ExitRoom':
            print("------ ALLOWED MOVEMENT ------")
            trace_items.append(">>> ALLOWED MOVEMENT.")

    def ReleaseResources(self):
        # ToDo: implement this. Call this method prior to exit of the StateMachin's endless loop
        pass

    # Main exec loop

    def run_fsm(self):
        print(">>> Start FSM")
        # state = ['Start', 'EnterRoom', 'ApproachDispenser', 'ApproachBed1', 'ApproachBed2', 'EnterToilet', 'ExitToilet', 'ExitRoom', 'Exit']
        while True:
            if self.state == 'Start':
                print("STATE :: Start")
                trace_items.append("SYSTEM STATUS: Start")
                self.checkRoomEnterConds()
                print(">>> self.checkRoomEnterConds()")
                # [DEBUG] print(inData)
            elif self.state == 'EnterRoom':
                print("STATE :: EnterRoom")
                trace_items.append("---------------------" + "\n" + "MOVEMENT STATUS: EnterRoom")
                self.checkStayInsideRoomConds()
                print(">>> self.checkStayInsideRoomConds()")
                # [DEBUG] print(inData)
            elif self.state == 'ApproachDispenser':
                print("STATE :: ApproachDispenser")
                trace_items.append("---------------------" + "\n" + "MOVEMENT STATUS: ApproachDispenser")
                self.checkPostDispenserConds()
                print(">>> self.checkPostDispenserConds()")
                # if True == self.checkPostDispenserCondsBed1():
                #    print(">>> self.checkPostDispenserCondsBed1()")
                # if True == self.checkPostDispenserCondsBed2():
                #    print(">>> self.checkPostDispenserCondsBed2()")
                # [DEBUG] print(inData)
            elif self.state == 'ApproachBed1':
                print("STATE :: ApproachBed1")
                trace_items.append("---------------------" + "\n" + "MOVEMENT STATUS: ApproachBed1")
                self.checkPostPatientBedConds()
                print(">>> self.checkPostPatientBedConds()")
                # self.checkPostPatientBed1Conds()
                # print(">>> self.checkPostPatientBed1Conds()")
                # [DEBUG] print(inData)
            elif self.state == 'ApproachBed2':
                print("STATE :: ApproachBed2")
                trace_items.append("---------------------" + "\n" + "MOVEMENT STATUS: ApproachBed2")
                self.checkPostPatientBedConds()
                print(">>> self.checkPostPatientBedConds()")
                # self.checkPostPatientBed2Conds()
                # print(">>> self.checkPostPatientBed2Conds()")
                # [DEBUG] print(inData)
            elif self.state == 'EnterToilet':
                print("STATE :: EnterToilet")
                trace_items.append("---------------------" + "\n" + "MOVEMENT STATUS: EnterToilet")
                self.checkStayInsideToiletConds()
                print(">>> self.checkStayInsideToiletConds()")
                # [DEBUG] print(inData)
            elif self.state == 'ExitToilet':
                print("STATE :: ExitToilet")
                trace_items.append("---------------------" + "\n" + "MOVEMENT STATUS: ExitToilet")
                self.checkPostToiletConds()
                print(">>> self.checkPostToiletConds()")
                # [DEBUG] print(inData)
            elif self.state == 'ExitRoom':
                print("STATE :: ExitRoom")
                trace_items.append("---------------------" + "\n" + "MOVEMENT STATUS: ExitRoom")
                # Extra treatment for 'Exit' inData element
                if inData.__contains__('Exit'):
                    inData.remove('Exit')
                # [DEBUG] print(inData)
                self.state = 'Stop'
            elif self.state == 'Stop':
                print("STATE :: Stop")
                trace_items.append("SYSTEM STATUS: Stop")
                # 1. Perform ACTIONS + release all (!!!), 2. exit endless loop -- due to EXIT condition!
                # [DEBUG] print(inData)
                print(">>> bye bye")
                print("")
                break
            else:
                # Perform ERROR HANDLING and exit endless loop
                # [DEBUG] print(inData)
                print(">>> bye bye")
                print("")
                break


# All-over exec loop

# print("+=+=+=+ Create nameless instance of FSM")
# myStateMachine_0 = StateMachine(None)
print("++++++ Running FSM's IN-Stream data input (as a thread) and split it up per badge")
mixed_in_stream_thread = Thread(target=createInStreamLists)
mixed_in_stream_thread.start()

# Wait 5 secs
print("Wait ...")
time.sleep(5)

# (2A) "Personalize" the badge (assign name "George" + trigger FSM
print()
print("++++++ Instanziate the SatusMed Finite State-Machine (FSM) as a class-thread -- 1st run")
myBadge = HasBadge("George")
# [Debug] print (">>>>>> BAGDE USER: " + str(myBadge.retName()) + " <<<<<<")
# badge_name = "badge_of_" + str(myBadge.retName())
# [Debug] print (">>>>>> USER's Badge name: " + badge_name + " <<<<<<")
# Trace user name to report file
trace_items.append("--------------------------------------- \n >>>> USER NAME (BADGE): " + str(
    myBadge.retName()) + " <<<< \n--------------------------------------- \n")

# (2B) Call first FSM instance
print("+++ Create first instance of FSM and run it ...")
myStateMachine_1 = StateMachine(myBadge.retName())
print("+++ Assign data to process to first instance of FSM:")
print(">>>>>> Data assigned: inData_badge1")
inData = inData_badge1
print("+++ Run first instance of FSM")
print()
myStateMachine_1.run_fsm()

# Wait 5 secs
print("Wait ...")
time.sleep(5)

print()
print("++++++ Instanziate the SatusMed Finite State-Machine (FSM) as a class-thread -- 2nd run")
myBadge = HasBadge("Maria")
# Trace user name to report file
trace_items.append("--------------------------------------- \n >>>> USER NAME (BADGE): " + str(
    myBadge.retName()) + " <<<< \n--------------------------------------- \n")

# (3B) Call second FSM instance
print("+++ Create second instance of FSM and run it ...")
myStateMachine_2 = StateMachine(myBadge.retName())
print("+++ Assign data to process to second instance of FSM:")
print(">>>>>> Data assigned: inData_badge3")
inData = inData_badge3
print("+++ Run second instance of FSM")
print()
myStateMachine_2.run_fsm()

# Wait 5 secs
print("Wait ...")
time.sleep(5)

print()
print("++++++ Instanziate the SatusMed Finite State-Machine (FSM) as a class-thread -- 3rd run")
myBadge = HasBadge("Roberto")
# Trace user name to report file
trace_items.append("--------------------------------------- \n >>>> USER NAME (BADGE): " + str(
    myBadge.retName()) + " <<<< \n--------------------------------------- \n")

# (4B) Call third FSM instance
print("+++ Create third instance of FSM and run it ...")
myStateMachine_3 = StateMachine(myBadge.retName())
print("+++ Assign data to process to third instance of FSM:")
print(">>>>>> Data assigned: inData_badge2")
inData = inData_badge2
print("+++ Run third instance of FSM")
print()
myStateMachine_3.run_fsm()

# Wait 5 secs
print("Wait ...")
time.sleep(5)

print()
print("++++++ Instanziate the SatusMed Finite State-Machine (FSM) as a class-thread -- 4th run")
myBadge = HasBadge("Rachel")
# Trace user name to report file
trace_items.append("--------------------------------------- \n >>>> USER NAME (BADGE): " + str(
    myBadge.retName()) + " <<<< \n--------------------------------------- \n")

# (5B) Call forth FSM instance
print("+++ Create forth instance of FSM and run it ...")
myStateMachine_4 = StateMachine(myBadge.retName())
print("+++ Assign data to process to forth instance of FSM:")
print(">>>>>> Data assigned: inData_badge4")
inData = inData_badge4
print("+++ Run forth instance of FSM")
print()
myStateMachine_4.run_fsm()

# (6A) Emulate WSS Stream and HTTPS RESTful interface, just running its "core", the Event Listener
print("++++++ Running EVENT LISTENERs, each one as per a proprietary FsmThread -- start and stop those")
WSSandHTTPSactor()

# (6B) Emulate event generation
print(">>> Start eventGenerator")
t_end = time.time() + eventGenerationTime
while time.time() < t_end:
    eventGenerator('')
print(">>> " + str(eventGenerationTime) + " seconds expired: stop eventGenerator")
eventGenerator('end')

# (7) Initiate data logging -- note: emulate DB information storage (very basic, though)
print()
print("++++++ System and flow data logging, report generation")
myDataLog = DataStorage(trace_items)
myDataLog.dataLogger()
print()
