=======================================
[Timestamp] : Thu Jan 11 17:53:38 2018
--- Report File ---
=======================================

--------------------------------------- 
 >>>> USER NAME (BADGE): George <<<< 
--------------------------------------- 

SYSTEM STATUS: Start
SYSTEM ALERTS:
   Enter-Room
   Motion-RoomDoor
   Proximity-Badge1-RoomEnter_S
---------------------
MOVEMENT STATUS: EnterRoom
SYSTEM ALERTS:
   Proximity-Badge1-AtBed1
>>> FORBIDDEN: User directly moves from room entrance to a patient bed!
---------------------
MOVEMENT STATUS: ApproachBed1
+++ DISPENSER STATUS: identified as used -- OK
SYSTEM ALERTS:
   Motion-Dispenser
   Motion-Badge1-AtDispenser
   Proximity-Badge1-AtDispenser
>>> ALLOWED MOVEMENT.
---------------------
MOVEMENT STATUS: ApproachDispenser
SYSTEM ALERTS:
   Proximity-Badge1-AtBed2
>>> ALLOWED MOVEMENT.
---------------------
MOVEMENT STATUS: ApproachBed2
+++ DISPENSER STATUS: identified as used -- OK
SYSTEM ALERTS:
   Motion-Dispenser
   Motion-Badge1-AtDispenser
   Proximity-Badge1-AtDispenser
>>> ALLOWED MOVEMENT.
---------------------
MOVEMENT STATUS: ApproachDispenser
+++ PATIENT STATUS: Patient ONE gets treated
SYSTEM ALERTS:
   Proximity-Badge1-AtBed1
>>> ALLOWED MOVEMENT.
---------------------
MOVEMENT STATUS: ApproachBed1
+++ PATIENT STATUS: Patient TWO gets treated
SYSTEM ALERTS:
   Proximity-Badge1-AtBed2
>>> FORBIDDEN: User moves between patient beds !
---------------------
MOVEMENT STATUS: ApproachBed2
+++ DISPENSER STATUS: identified as used -- OK
SYSTEM ALERTS:
   Motion-Dispenser
   Motion-Badge1-AtDispenser
   Proximity-Badge1-AtDispenser
>>> ALLOWED MOVEMENT.
---------------------
MOVEMENT STATUS: ApproachDispenser
+++ PATIENT STATUS: Patient ONE gets treated
SYSTEM ALERTS:
   Proximity-Badge1-AtBed1
>>> ALLOWED MOVEMENT.
---------------------
MOVEMENT STATUS: ApproachBed1
+++ DISPENSER STATUS: identified as used -- OK
SYSTEM ALERTS:
   Motion-Dispenser
   Motion-Badge1-AtDispenser
   Proximity-Badge1-AtDispenser
>>> ALLOWED MOVEMENT.
---------------------
MOVEMENT STATUS: ApproachDispenser
SYSTEM ALERTS:
   Motion-ToiletDoor
   Proximity-Badge1-ToiletEnter_S
>>> ALLOWED MOVEMENT.
---------------------
MOVEMENT STATUS: EnterToilet
SYSTEM ALERTS:
   Motion-ToiletDoor
   Motion-Badge1-AtToiletDoor
   Proximity-Badge1-ToiletEnter_C
>>> ALLOWED MOVEMENT.
---------------------
MOVEMENT STATUS: ExitToilet
SYSTEM ALERTS:
   Proximity-Badge1-AtBed2
>>> FORBIDDEN: User exits from toilet and goes directly to a patient bed!
---------------------
MOVEMENT STATUS: ApproachBed2
+++ DISPENSER STATUS: identified as used -- OK
SYSTEM ALERTS:
   Motion-Dispenser
   Motion-Badge1-AtDispenser
   Proximity-Badge1-AtDispenser
>>> ALLOWED MOVEMENT.
---------------------
MOVEMENT STATUS: ApproachDispenser
+++ PATIENT STATUS: Patient ONE gets treated
SYSTEM ALERTS:
   Proximity-Badge1-AtBed1
>>> ALLOWED MOVEMENT.
---------------------
MOVEMENT STATUS: ApproachBed1
+++ DISPENSER STATUS: identified as used -- OK
SYSTEM ALERTS:
   Motion-Dispenser
   Motion-Badge1-AtDispenser
   Proximity-Badge1-AtDispenser
>>> ALLOWED MOVEMENT.
---------------------
MOVEMENT STATUS: ApproachDispenser
+++ PATIENT STATUS: Patient TWO gets treated
SYSTEM ALERTS:
   Proximity-Badge1-AtBed2
>>> ALLOWED MOVEMENT.
---------------------
MOVEMENT STATUS: ApproachBed2
+++ DISPENSER STATUS: identified as used -- OK
SYSTEM ALERTS:
   Motion-Dispenser
   Motion-Badge1-AtDispenser
   Proximity-Badge1-AtDispenser
>>> ALLOWED MOVEMENT.
---------------------
MOVEMENT STATUS: ApproachDispenser
SYSTEM ALERTS:
   Motion-RoomDoor
   Motion-Badge1-AtRoomDoor
   Exit-Room
   Proximity-Badge1-RoomEnter_C
>>> ALLOWED MOVEMENT.
---------------------
MOVEMENT STATUS: ExitRoom
SYSTEM STATUS: Stop

=======================================

--------------------------------------- 
 >>>> USER NAME (BADGE): Maria <<<< 
--------------------------------------- 

SYSTEM STATUS: Start
SYSTEM ALERTS:
   Enter-Room
   Motion-RoomDoor
   Proximity-Badge3-RoomEnter_S
---------------------
MOVEMENT STATUS: EnterRoom
SYSTEM ALERTS:
   Motion-ToiletDoor
   Proximity-Badge3-ToiletEnter_S
---------------------
MOVEMENT STATUS: EnterToilet
SYSTEM ALERTS:
   Motion-ToiletDoor
   Motion-Badge3-AtToiletDoor
   Proximity-Badge3-ToiletEnter_C
>>> ALLOWED MOVEMENT.
---------------------
MOVEMENT STATUS: ExitToilet
+++ DISPENSER STATUS: identified as used -- OK
SYSTEM ALERTS:
   Motion-Dispenser
   Motion-Badge3-AtDispenser
   Proximity-Badge3-AtDispenser
+++ PATIENT STATUS: Patient TWO gets treated
---------------------
MOVEMENT STATUS: ApproachDispenser
SYSTEM ALERTS:
   Proximity-Badge3-AtBed2
>>> ALLOWED MOVEMENT.
---------------------
MOVEMENT STATUS: ApproachBed2
+++ DISPENSER STATUS: identified as used -- OK
SYSTEM ALERTS:
   Motion-Dispenser
   Motion-Badge3-AtDispenser
   Proximity-Badge3-AtDispenser
>>> ALLOWED MOVEMENT.
---------------------
MOVEMENT STATUS: ApproachDispenser
SYSTEM ALERTS:
   Proximity-Badge3-AtBed1
>>> ALLOWED MOVEMENT.
---------------------
MOVEMENT STATUS: ApproachBed1
SYSTEM ALERTS:
   Motion-RoomDoor
   Motion-Badge3-AtRoomDoor
   Exit-Room
   Proximity-Badge3-RoomEnter_C
>>> ALLOWED MOVEMENT.
---------------------
MOVEMENT STATUS: ExitRoom
SYSTEM STATUS: Stop

=======================================

--------------------------------------- 
 >>>> USER NAME (BADGE): Roberto <<<< 
--------------------------------------- 

SYSTEM STATUS: Start
SYSTEM ALERTS:
   Enter-Room
   Motion-RoomDoor
   Proximity-Badge2-RoomEnter_S
---------------------
MOVEMENT STATUS: EnterRoom
+++ DISPENSER STATUS: identified as used -- OK
SYSTEM ALERTS:
   Motion-Dispenser
   Motion-Badge2-AtDispenser
   Proximity-Badge2-AtDispenser
+++ PATIENT STATUS: Patient ONE gets treated
---------------------
MOVEMENT STATUS: ApproachDispenser
SYSTEM ALERTS:
   Proximity-Badge2-AtBed1
>>> ALLOWED MOVEMENT.
---------------------
MOVEMENT STATUS: ApproachBed1
SYSTEM ALERTS:
   Motion-RoomDoor
   Motion-Badge2-AtRoomDoor
   Exit-Room
   Proximity-Badge2-RoomEnter_C
>>> ALLOWED MOVEMENT.
---------------------
MOVEMENT STATUS: ExitRoom
SYSTEM STATUS: Stop

=======================================

--------------------------------------- 
 >>>> USER NAME (BADGE): Rachel <<<< 
--------------------------------------- 

SYSTEM STATUS: Start
SYSTEM ALERTS:
   Enter-Room
   Motion-RoomDoor
   Proximity-Badge4-RoomEnter_S
---------------------
MOVEMENT STATUS: EnterRoom
SYSTEM ALERTS:
   Motion-RoomDoor
   Exit-Room
   Proximity-Badge4-RoomEnter_C
---------------------
MOVEMENT STATUS: ExitRoom
SYSTEM STATUS: Stop

=======================================

