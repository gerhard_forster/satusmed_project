import websocket as ws


class Stream:
    def __init__(self, host, key):
        self.host = host
        self.key = key
        self.connection = None

    def connect(self):
        print(">>> [Stream] --> Connecting >>> ")
        self.connection = ws.create_connection(self.host, header={"BZID": self.key})

    def run(self, on_message=None, on_error=None, on_close=None):
        self.__end_connection = False
        self.app = ws.WebSocketApp(self.host,
                                   on_message=on_message,
                                   on_error=on_error,
                                   on_close=on_close,
                                   header={"BZID": self.key})

        self.app.run_forever()

    def close_stream(self):
        self.app.keep_running = False
