import time


class Log:
    def __init__(self, data):
        self.data = data
        self.date = time.asctime(time.localtime(time.time()))

    def toString(self):
        return "[Error] >> Data: "+str(self.data)+", Date: "+str(self.date)
