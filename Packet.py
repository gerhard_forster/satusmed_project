class ScanData:
    def __init__(self, data):
        self.__data = data

    def rawString(self):
        return str(self.__data)

    def toString(self):
        out = "PacketType: ScanData, Blufi: " + self.__data["adapterId"]
        if "id" in self.__data:
            out += ", Beacon: " + str(self.__data["id"]) + ", Rssi: " + str(self.__data["rssi"]) + ", Time: " + str(
                self.__data["timestamp"])
        return out


class Location:
    def __init__(self, data):
        self.__data = data


class Policy:
    def __init__(self, data):
        self.__data = data
