# No imports required.


# CLASS BluFi

class BluFi:
    def __init__(self, info):
        print(">>> [BluFi]: Constructor.")
        self.info = info


# CLASS Beacon

class Beacon:
    def __init__(self, info):
        print(">>> [Beacon]: Constructor.")
        self.info = info

    def update_data(self, data):
        if "scanData" in self.info:
            if "battery" in self.info["scanData"] and "battery" in data:
                self.info["scanData"]["battery"] = data["battery"]
            if "temp" in self.info["scanData"] and "temp" in data:
                self.info["scanData"]["temp"] = data["temp"]
            if "blufiId" in self.info["scanData"] and "blufiId" in data:
                self.info["scanData"]["blufiId"] = data["blufiId"]
            if "rssi" in self.info["scanData"] and "rssi" in data:
                self.info["scanData"]["rssi"] = data["rssi"]

# Nothing directly executed.
