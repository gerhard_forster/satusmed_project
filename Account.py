import requests
# Will be used later: import json
from Session import Session
import Devices
import Location


# Retrieve objetc list (GET) to use (PUT / POST / ...)
blufi_objects = list()
one_blufi_object = {'deviceId': '9449', 'projectId': 3267, 'virtualDeviceId': None, 'name': 'BF-EnterRoom', 'status': 'CONNECTED', 'notes': '', 'bleFirmware': {'firmwareId': 3109, 'type': 'Blufi PG2', 'version': 398, 'deviceType': {'deviceTypeId': 42, 'name': 'Blufi PG2', 'code': 'BLE2WIFI_CC_PG2', 'active': False, 'blufi': True}}, 'deviceType': {'deviceTypeId': 42, 'name': 'Blufi PG2', 'code': 'BLE2WIFI_CC_PG2', 'active': False, 'blufi': True}, 'configuration': None, 'macAddress': 'f4:5e:ab:15:b4:91', 'altitude': 88.9957, 'latitude': -30.03120445126471, 'longitude': -51.19290021745559, 'tags': [], 'selfie': None, 'scanData': {'beaconId': '4473071571180939690', 'blufiId': 9449, 'rssi': -87, 'temp': 27.25, 'battery': 3630, 'timestamp': 1516982200716, 'latitude': None, 'longitude': None, 'status': None, 'beaconName': None, 'blufiName': None}, 'totalViolations': 0, 'violations': [], 'locationId': 3332, 'autoLocationName': None, 'dateProvisioned': 1508891065000, 'dateUpdated': 1516925636000, 'dateCreated': 1508890943000, 'wifiFirmwareRev': 33, 'sid64': '1948851865696069643', 'sid128': None, 'edgeDeviceId': None, 'bleMac': '01:d2:4c:6c:ce:79:87', 'ipAddress': '192.168.0.6', 'gwAddress': '192.168.0.1', 'dnsAddress': '201.21.192.123', 'cloudAddress': '2054515810', 'blufiTemplateId': None, 'wifiSsid': 'HomeP', 'blufiTemplateName': 'SatusMed-HomeP', 'wifiTemplateSsid': 'HomeP', 'wifiTemplateSecurityType': 'wpa2', 'wsEndpoint': None, 'locations': None, 'defaultScale': 1.5, 'defaultOffset': -70.0, 'defaultVariance': 9.0, 'boxCalibrationScale': None, 'boxCalibrationScaleOriginal': None, 'boxCalibrationOffset': None, 'boxCalibrationOffsetOriginal': None, 'boxCalibrationVariance': None, 'boxCalibrationVarianceOriginal': None, 'humanCalibrationScale': -25263577493228.676, 'humanCalibrationScaleOriginal': -25263577493228.676, 'humanCalibrationOffset': -182492952648532.7, 'humanCalibrationOffsetOriginal': -182492952648532.7, 'humanCalibrationVariance': 28.310229445506693, 'humanCalibrationVarianceOriginal': 28.310229445506693, 'metalCalibrationScale': None, 'metalCalibrationScaleOriginal': None, 'metalCalibrationOffset': None, 'metalCalibrationOffsetOriginal': None, 'metalCalibrationVariance': None, 'metalCalibrationVarianceOriginal': None, 'assetTrackable': True, 'assetTrackRssiOffset': 0.0, 'assetTrackAltitude': 88.9957, 'assetTrackLatitude': -30.03120445126471, 'assetTrackLongitude': -51.19290021745559, 'scanDataMap': None, 'scanMode': {'scanModeId': 11, 'name': 'System Cloud Scan Mode', 'description': 'Optimized for remote beacon configuration', 'assetTrackingMode': False, 'deviceJobMode': False, 'defaultMode': True, 'bulkMode': False, 'scanInterval': 80, 'scanWindow': 80, 'channels': '2491,2496,2498,255,255,255', 'channelCycles': '5,1,5,0,0,0', 'channelCount': 6, 'trackingChannels': '', 'idleCycles': 0, 'scanMode': 0, 'wifiProbe': False, 'startFrequency': 0, 'frequencyStep': 0, 'sweepChannelCount': 0, 'sweepMode': 0, 'dwellTime': 24, 'scanDelayTime': -11, 'maxNoise': 0, 'minSnr': 0, 'maxSnr': 0, 'wifiProbeChannel': 6, 'wifiProbeInterval': 0, 'wifiProbeWindow': 0, 'dateCreated': 1504621698000, 'dateUpdated': None}, 'jobScanMode': None, 'stat': None, 'sleepSchedule': None, 'sleepCycle': None, 'configData': '{"pairingRssi":-32,"rttEnabled":true,"cloudUrl":"wss://bluzone.io:443","cloudUnSecureUrl":"ws://bluzone.io:80","cloudCert":null,"profileData":{"name":"SatusMed-HomeP","description":"","ssid":"HomeP","securityType":"wpa2","passphrase":"RocketMan2013!","channels":"1,2,3,4,5,6,7,8,9,10,11,12,13,14","rssiThreshold":-99,"user":null,"wifiCert":null},"aggregator":{"maxTimeInMs":250,"maxBufferSize":4096},"beacon":{"intervalDayInSeconds":2.0,"intervalNightInSeconds":2.0,"txPowerDay":-20,"txPowerNight":-20,"channelsDay":"Jb+Z","channelsNight":"Jb+Z","num":3,"type":1},"rxGain":0,"antenna":0}', 'configVersion': 1, 'websocketSessionHostname': 'bluzone.io', 'websocketSessionId': 'a98cdc00-a7a8-4386-6ed3-d9a6b06493e9', 'deviceJobsEnabled': True, 'streamProduceEnabled': True, 'batteryBlufi': False, 'batteryBlufiAntenna': 'HORIZONTAL', 'rxGainEnabled': False, 'iBeaconConfiguration': {'iBeaconUuidAssignmentType': 'PROJECT', 'iBeaconUuidValue': None, 'iBeaconMajorValue': None, 'iBeaconMinorValue': None, 'iBeaconMajorAuto': True, 'iBeaconMinorAuto': True, 'iBeaconPower': -20, 'iBeaconInterval': 0.1, 'iBeaconNightPower': -20, 'iBeaconNightInterval': 0.1, 'iBeacon': None}}
beacon_objects = list()
one_beacon_object = {'deviceId': '6196330507182129751', 'projectId': 3267, 'virtualDeviceId': None, 'name': 'BLEBADGE-User1', 'status': 'ACTIVE', 'notes': None, 'bleFirmware': {'firmwareId': 2756, 'type': 'Beacon Card V2', 'version': 404, 'deviceType': {'deviceTypeId': 46, 'name': 'Beacon Card V2', 'code': 'TAG_CARD_V2', 'active': True, 'blufi': False}}, 'deviceType': {'deviceTypeId': 46, 'name': 'Beacon Card V2', 'code': 'TAG_CARD_V2', 'active': True, 'blufi': False}, 'configuration': {'projectId': 3267, 'deviceType': {'deviceTypeId': 46, 'name': 'Beacon Card V2', 'code': 'TAG_CARD_V2', 'active': True, 'blufi': False}, 'name': 'Default Beacon Card V2 Template - Asset Tracking Calibration', 'description': 'System created default template', 'beaconLock': False, 'sBeacon': True, 'sBeaconPower': -4, 'sBeaconInterval': 10.0, 'sBeaconNightPower': -4, 'sBeaconNightInterval': 10.0, 'sBeaconTracking': True, 'sBeaconTrackingPower': -4, 'sBeaconTrackingInterval': 1.0, 'sBeaconTrackingNightPower': -4, 'sBeaconTrackingNightInterval': 1.0, 'iBeacon': False, 'iBeaconPower': 0, 'iBeaconInterval': 0.0, 'iBeaconNightPower': 0, 'iBeaconNightInterval': 0.0, 'iBeaconUuid': '57e2ea03-2255-41eb-82da-c4089a5b4fad', 'iBeaconMajor': 22157, 'iBeaconMinor': 20558, 'motion': False, 'motionPower': 0, 'motionInterval': 0.0, 'motionNightPower': 0, 'motionNightInterval': 0.0, 'motionConfiguration': {'motionConfigId': None, 'enabled': False, 'fullScale': 8, 'motionEnabled': False, 'motionX': False, 'motionY': False, 'motionZ': False, 'transEnabled': False, 'transX': False, 'transY': False, 'transZ': False, 'enableHpf': False, 'motionThr': 3, 'motionDbn': 2, 'transThr': 3, 'transDbn': 2, 'rate': 7, 'cutoff': 0, 'advCycle': 5, 'mcRate': 5, 'mcStandbyRate': 0, 'mcSniffRate': 4, 'mcPowerMode': 0}, 'swMotionConfiguration': {'swMotionConfigId': 2, 'xAxisFilter': {'swMotionFilterConfigId': 1, 'filter1_b0': 0.7939064395613799, 'filter1_b1': -1.58781287912276, 'filter1_b2': 0.7939064395613799, 'filter1_a0': 1.694322316954193, 'filter1_a1': -0.7210203914385739, 'filter2_b0': 1, 'filter2_b1': -2, 'filter2_b2': 1, 'filter2_a0': 1.845086624729728, 'filter2_a1': -0.8741603491588094}, 'yAxisFilter': {'swMotionFilterConfigId': 1, 'filter1_b0': 0.7939064395613799, 'filter1_b1': -1.58781287912276, 'filter1_b2': 0.7939064395613799, 'filter1_a0': 1.694322316954193, 'filter1_a1': -0.7210203914385739, 'filter2_b0': 1, 'filter2_b1': -2, 'filter2_b2': 1, 'filter2_a0': 1.845086624729728, 'filter2_a1': -0.8741603491588094}, 'zAxisFilter': {'swMotionFilterConfigId': 1, 'filter1_b0': 0.7939064395613799, 'filter1_b1': -1.58781287912276, 'filter1_b2': 0.7939064395613799, 'filter1_a0': 1.694322316954193, 'filter1_a1': -0.7210203914385739, 'filter2_b0': 1, 'filter2_b1': -2, 'filter2_b2': 1, 'filter2_a0': 1.845086624729728, 'filter2_a1': -0.8741603491588094}, 'counter0': {'swMotionCounterConfigId': 7, 'inputSkip': 2, 'startOnEvent': True, 'continuous': True, 'countOnAxes': 'x,y,z,xy,xz,yz,xyz', 'input1': {'swMotionCounterInputId': 7, 'source': 'x', 'countLow': 1, 'countHigh': 1, 'levelLow': 0.000747680664063, 'levelHigh': 0.000762939453125, 'signedComparison': False, 'countDown': False}, 'input2': {'swMotionCounterInputId': 8, 'source': 'y', 'countLow': 1, 'countHigh': 1, 'levelLow': 0.000747680664063, 'levelHigh': 0.000762939453125, 'signedComparison': False, 'countDown': False}, 'input3': {'swMotionCounterInputId': 9, 'source': 'z', 'countLow': 1, 'countHigh': 1, 'levelLow': 0.000747680664063, 'levelHigh': 0.000762939453125, 'signedComparison': False, 'countDown': False}}, 'counter1': None, 'counter2': None, 'counter3': None, 'counter4': None, 'counter5': None}, 'motionConditionConfiguration': None, 'spectrum': False, 'spectrumPower': 0, 'spectrumInterval': 0.0, 'spectrumNightPower': 0, 'spectrumNightInterval': 0.0, 'dataFountain': False, 'dataFountainPower': 0, 'dataFountainInterval': 0.0, 'dataFountainNightPower': 0, 'dataFountainNightInterval': 0.0, 'rawSensor': False, 'rawSensorPower': 0, 'rawSensorInterval': 0.0, 'rawSensorNightPower': 0, 'rawSensorNightInterval': 0.0, 'light': False, 'lightPower': 0, 'lightInterval': 0.0, 'lightNightPower': 0, 'lightNightInterval': 0.0, 'eddystoneUid': False, 'eddystoneUidPower': 0, 'eddystoneUidInterval': 0.0, 'eddystoneUidNightPower': 0, 'eddystoneUidNightInterval': 0.0, 'eddystoneUidNamespace': None, 'eddystoneUidInstance': None, 'eddystoneUidInstanceAuto': True, 'eddystoneUrl': False, 'eddystoneUrlPower': 0, 'eddystoneUrlInterval': 0.0, 'eddystoneUrlNightPower': 0, 'eddystoneUrlNightInterval': 0.0, 'eddystoneUrlUrl': None, 'eddystoneTlm': False, 'eddystoneTlmPower': 0, 'eddystoneTlmInterval': 0.0, 'eddystoneTlmNightPower': 0, 'eddystoneTlmNightInterval': 0.0, 'pairingRssi': -99, 'datalog': False, 'datalogTemperatureInterval': 0, 'datalogAccelInterval': 0, 'datalogLightInterval': 0, 'datalogMagInterval': 0, 'templateId': 133774, 'tags': [], 'iBeaconUuidAssignmentType': 'PROJECT', 'iBeaconUuidValue': '57e2ea03-2255-41eb-82da-c4089a5b4fad', 'iBeaconMajorValue': 22157, 'iBeaconMinorValue': 20558, 'iBeaconMajorAuto': True, 'iBeaconMinorAuto': True, 'assetTrackingEnabled': True, 'parkingEnabled': False, 'parkingTrainingTimeInSec': 86400, 'metaData': None, 'policies': [], 'systemCreated': True, 'dateUpdated': 1507653175000, 'dateCreated': 1507653175000, 'accelReadConfig': None, 'firmwareConfig': None, 'datalogReadConfig': None, 'csTamperConfig': None}, 'macAddress': '01:c4:b1:55:a4:92:bf', 'altitude': 91.7775, 'latitude': -30.031237730367508, 'longitude': -51.192880645394325, 'tags': [], 'selfie': None, 'scanData': {'beaconId': '6196330507182129751', 'blufiId': 9449, 'rssi': -96, 'temp': 21.75, 'battery': 3000, 'timestamp': 1517241099767, 'latitude': None, 'longitude': None, 'status': None, 'beaconName': None, 'blufiName': None}, 'totalViolations': 0, 'violations': [], 'locationId': 3332, 'autoLocationName': None, 'dateProvisioned': 1508894981000, 'dateUpdated': 1512992682000, 'dateCreated': 1508894981000, 'dynamicUrl': None, 'blufi': None, 'templateId': 133774, 'locations': None, 'configVersion': 6, 'managerType': 'CLOUD', 'assetTrackable': True, 'assetMaterialType': 'HUMAN', 'onlineCalibrationReference': False, 'assetTrackAltitude': 91.7775, 'assetTrackLatitude': -30.031254751328714, 'assetTrackLongitude': -51.19260559686027, 'calibrationRssiOffset': 0.0, 'motionCalEnabled': False, 'motionCalApplied': 0, 'conditionSummaryInterval': 60, 'accelReadRate': 1, 'accelReadInterval': 2, 'accelReadSummaryInterval': 2, 'ambient': False, 'scanDataMap': None, 'metaData': None, 'stats': {'beaconId': '6196330507182129751', 'projectId': 3267, 'blufiId': 9451, 'rssi': -86, 'batteryVoltageMax': 3.05, 'batteryVoltage': 3.05, 'temperatureMax': 21.75, 'temperatureMin': 21.75, 'temperature': 21.75, 'illegalPackets': None, 'rmsX': None, 'rmsY': None, 'rmsZ': None, 'pkX': None, 'pkY': None, 'pkZ': None, 'pkpkX': None, 'pkpkY': None, 'pkpkZ': None, 'dutyCyclesTotal': None, 'dutyCycleDuration': None, 'violationsTotal': None, 'violationsActive': 0, 'violationDuration': None, 'dateSeen': 1517241139855, 'dateBatteryVoltage': 1517241139855, 'dateTemperatureMax': 1517184004646, 'dateTemperatureMin': 1517184004646, 'dateTemperature': 1517241139855, 'dateIllegalPacket': None, 'dateConditionSummary': None, 'dateViolated': None, 'dateCreated': 1517184004646, 'dateUpdated': 1517241148639}, 'id64Hex': '55FDC9C44781C257', 'configData': '{"projectId":3267,"deviceType":{"deviceTypeId":46,"name":"Beacon Card V2","code":"TAG_CARD_V2","active":true,"blufi":false},"name":"Default Beacon Card V2 Template - Asset Tracking Calibration","description":"System created default template","beaconLock":false,"sBeacon":true,"sBeaconPower":-4,"sBeaconInterval":10.0,"sBeaconNightPower":-4,"sBeaconNightInterval":10.0,"sBeaconTracking":true,"sBeaconTrackingPower":-4,"sBeaconTrackingInterval":1.0,"sBeaconTrackingNightPower":-4,"sBeaconTrackingNightInterval":1.0,"iBeacon":false,"iBeaconPower":0,"iBeaconInterval":0.0,"iBeaconNightPower":0,"iBeaconNightInterval":0.0,"iBeaconUuid":"57e2ea03-2255-41eb-82da-c4089a5b4fad","iBeaconMajor":22157,"iBeaconMinor":20558,"motion":false,"motionPower":0,"motionInterval":0.0,"motionNightPower":0,"motionNightInterval":0.0,"motionConfiguration":{"motionConfigId":null,"enabled":false,"fullScale":8,"motionEnabled":false,"motionX":false,"motionY":false,"motionZ":false,"transEnabled":false,"transX":false,"transY":false,"transZ":false,"enableHpf":false,"motionThr":3,"motionDbn":2,"transThr":3,"transDbn":2,"rate":7,"cutoff":0,"advCycle":5,"mcRate":5,"mcStandbyRate":0,"mcSniffRate":4,"mcPowerMode":0},"swMotionConfiguration":{"swMotionConfigId":2,"xAxisFilter":{"swMotionFilterConfigId":1,"filter1_b0":0.7939064395613799,"filter1_b1":-1.58781287912276,"filter1_b2":0.7939064395613799,"filter1_a0":1.694322316954193,"filter1_a1":-0.7210203914385739,"filter2_b0":1,"filter2_b1":-2,"filter2_b2":1,"filter2_a0":1.845086624729728,"filter2_a1":-0.8741603491588094},"yAxisFilter":{"swMotionFilterConfigId":1,"filter1_b0":0.7939064395613799,"filter1_b1":-1.58781287912276,"filter1_b2":0.7939064395613799,"filter1_a0":1.694322316954193,"filter1_a1":-0.7210203914385739,"filter2_b0":1,"filter2_b1":-2,"filter2_b2":1,"filter2_a0":1.845086624729728,"filter2_a1":-0.8741603491588094},"zAxisFilter":{"swMotionFilterConfigId":1,"filter1_b0":0.7939064395613799,"filter1_b1":-1.58781287912276,"filter1_b2":0.7939064395613799,"filter1_a0":1.694322316954193,"filter1_a1":-0.7210203914385739,"filter2_b0":1,"filter2_b1":-2,"filter2_b2":1,"filter2_a0":1.845086624729728,"filter2_a1":-0.8741603491588094},"counter0":{"swMotionCounterConfigId":7,"inputSkip":2,"startOnEvent":true,"continuous":true,"countOnAxes":"x,y,z,xy,xz,yz,xyz","input1":{"swMotionCounterInputId":7,"source":"x","countLow":1,"countHigh":1,"levelLow":0.000747680664063,"levelHigh":0.000762939453125,"signedComparison":false,"countDown":false},"input2":{"swMotionCounterInputId":8,"source":"y","countLow":1,"countHigh":1,"levelLow":0.000747680664063,"levelHigh":0.000762939453125,"signedComparison":false,"countDown":false},"input3":{"swMotionCounterInputId":9,"source":"z","countLow":1,"countHigh":1,"levelLow":0.000747680664063,"levelHigh":0.000762939453125,"signedComparison":false,"countDown":false}},"counter1":null,"counter2":null,"counter3":null,"counter4":null,"counter5":null},"motionConditionConfiguration":null,"spectrum":false,"spectrumPower":0,"spectrumInterval":0.0,"spectrumNightPower":0,"spectrumNightInterval":0.0,"dataFountain":false,"dataFountainPower":0,"dataFountainInterval":0.0,"dataFountainNightPower":0,"dataFountainNightInterval":0.0,"rawSensor":false,"rawSensorPower":0,"rawSensorInterval":0.0,"rawSensorNightPower":0,"rawSensorNightInterval":0.0,"light":false,"lightPower":0,"lightInterval":0.0,"lightNightPower":0,"lightNightInterval":0.0,"eddystoneUid":false,"eddystoneUidPower":0,"eddystoneUidInterval":0.0,"eddystoneUidNightPower":0,"eddystoneUidNightInterval":0.0,"eddystoneUidNamespace":null,"eddystoneUidInstance":null,"eddystoneUidInstanceAuto":true,"eddystoneUrl":false,"eddystoneUrlPower":0,"eddystoneUrlInterval":0.0,"eddystoneUrlNightPower":0,"eddystoneUrlNightInterval":0.0,"eddystoneUrlUrl":null,"eddystoneTlm":false,"eddystoneTlmPower":0,"eddystoneTlmInterval":0.0,"eddystoneTlmNightPower":0,"eddystoneTlmNightInterval":0.0,"pairingRssi":-99,"datalog":false,"datalogTemperatureInterval":0,"datalogAccelInterval":0,"datalogLightInterval":0,"datalogMagInterval":0,"templateId":133774,"tags":[],"iBeaconUuidAssignmentType":"PROJECT","iBeaconUuidValue":"57e2ea03-2255-41eb-82da-c4089a5b4fad","iBeaconMajorValue":22157,"iBeaconMinorValue":20558,"iBeaconMajorAuto":true,"iBeaconMinorAuto":true,"assetTrackingEnabled":true,"parkingEnabled":false,"parkingTrainingTimeInSec":86400,"metaData":null,"policies":[],"systemCreated":true,"dateUpdated":1507653175000,"dateCreated":1507653175000,"accelReadConfig":null,"firmwareConfig":null,"datalogReadConfig":null,"csTamperConfig":null}', 'parkingEnabled': False, 'parkingStatus': 'NOT_AVAILABLE', 'parkingStatusChange': 1510595982000, 'parkingTrainingTimeInSec': 86400, 'systemParkingTrainingTimeInSec': 0}
policy_objects = list()
one_policy_id = 5373
one_policy_object = {'policyId': 5373, 'projectId': 3267, 'locationId': None, 'globalScope': False, 'name': 'Location-Badge3', 'description': 'Idea: locate User #3 utilizing Badge #3 relative to reference points in the room.', 'namespace': 'BEACON', 'metricType': 'PRESENCE', 'operatorType': 'GREATER_THAN_OR_EQUALS', 'duration': None, 'sampleInterval': None, 'threshold': 1.0, 'thresholdHyst': None, 'thresholdRange': [None, None], 'regions': None, 'blufisInZone': None, 'zoneBeacons': [], 'period': 60, 'emailAddresses': [], 'managedBeaconIds': None, 'managedBlufiIds': None, 'anchorBlufis': [{'deviceId': 9449, 'sid64': '1948851865696069643', 'bluetoothMac': None, 'wifiFWRev': None, 'bluetoothFWRev': None, 'ipAddress': '192.168.0.6', 'gwIpAddress': None, 'dnsIpAddress': '201.21.192.123', 'wifiMac': None, 'cloudUrl': None, 'ssid': None, 'altitude': 88.9957, 'latitude': -30.03120445126471, 'longitude': -51.19290021745559, 'resetReason': None}, {'deviceId': 9450, 'sid64': '5691195718672170288', 'bluetoothMac': None, 'wifiFWRev': None, 'bluetoothFWRev': None, 'ipAddress': '192.168.0.13', 'gwIpAddress': None, 'dnsIpAddress': '201.21.192.119', 'wifiMac': None, 'cloudUrl': None, 'ssid': None, 'altitude': 87.5229, 'latitude': -30.031276123180803, 'longitude': -51.19288005870874, 'resetReason': None}, {'deviceId': 9451, 'sid64': '2235332527816401429', 'bluetoothMac': None, 'wifiFWRev': None, 'bluetoothFWRev': None, 'ipAddress': '192.168.0.4', 'gwIpAddress': None, 'dnsIpAddress': '201.21.192.123', 'wifiMac': None, 'cloudUrl': None, 'ssid': None, 'altitude': 91.2554, 'latitude': -30.031275844148453, 'longitude': -51.19285884598878, 'resetReason': None}, {'deviceId': 9452, 'sid64': '14126010361715792962', 'bluetoothMac': None, 'wifiFWRev': None, 'bluetoothFWRev': None, 'ipAddress': '192.168.0.18', 'gwIpAddress': None, 'dnsIpAddress': '201.21.192.119', 'wifiMac': None, 'cloudUrl': None, 'ssid': None, 'altitude': 88.4911, 'latitude': -30.031255937721767, 'longitude': -51.19283558164307, 'resetReason': None}], 'entryBlufi': None, 'exitBlufi': None, 'interior': False, 'policyTrigger': 'ENTER_EXIT', 'conditionModelTemplate': None, 'dwellMonitorSettings': [], 'deviceCount': 1, 'bounds': None, 'boundsGeojson': None, 'autoClearEnabled': None, 'varianceThreshold': 0, 'autoClearType': 'BY_METRIC', 'autoClearOperatorType': 'GREATER_THAN_OR_EQUALS', 'autoClearTimeout': None, 'autoClearRssiThreshold': None, 'deviceJobSchedules': [], 'shopperTrackEventId': None, 'dateUpdated': 1513277982000, 'dateCreated': 1510597195000, 'missingDataSeconds': 300, 'missingDataType': 'STAY_THE_SAME'}


# CLASS Account

class Account:
    # Account construtor (use account key, Project ID, user and password)
    def __init__(self, key, project_id, usr, ps):
        print(">>> [Account]: Constructor.")
        self.key = key
        self.project_id = project_id
        self.blufis = None
        self.beacons = None
        self.session = Session(usr, ps)
        self.host = "https://bluzone.io/portal"
        # Create empty lists of: locations, location (both BluFi, effectively), beacon and blufi locations and location data
        self.location_data = []
        self.blufi_locations = []
        self.beacon_locations = []
        self.location = []
        self.locations = []
        self.location_id = []

    # Request receiving all devices associated with the account + create a list of Beacons and BluFis
    def get_devices(self):
        self.get_beacons()
        self.get_blufis()

    # (A) ACTIVATE / DEACTIVATE DEVICES ==> no option of tagging a device as 'inactive' existing in Bluzone.
    #                                       Confirmed by Bluvision support --> NOT POSSIBLE !!!

    # (B) CONFIGURE / RECONFIGURE DEVICES -- part (I) BluFis ["deviceId" mostly needed]

    # (B1.1) This REST API gets all Blufi objects and related Blufi data
    def get_blufis(self):
        # [4.6.1] Get Blufis --> GET /papis/v1/projects/{projectId}/devices/blufis
        r = requests.get(self.host + "/papis/v1/projects/" + self.project_id + "/devices/blufis",
                         headers={'BZID': self.key, 'Accept': 'application/json'})
        r = r.json()
        self.blufis = dict()
        global blufi_objects
        for x in r:
            if "deviceId" in x:
                self.blufis[x["deviceId"]] = Devices.BluFi(x)
                blufi_objects.append(x)
            # [Debug] print(blufi_objects)

    # (B1.2) This REST API may be used to overcome Blufi’s BLE instabilities (if occurring).
    # The reset will force a Blufi to restart internally and reconnect to Bluzone.
    # Note: Wifi reset is not supported by the Blufi hardware
    def reset_all_blufi_ble(self):
        # [4.6.2] Reset Blufi BLE --> PUT /papis/v1/projects/{projectId}/devices/blufis/_bleReset ==> ERROR CODE 500
        blufiId_list = []    # Create a BlufiId list (IDs == true int !)
        blufiId_array = ""   # Create a BlufiId array without [ and ] (IDs == str without ' and ')
        # Loop over all Blufis available in order to create a Blufi ID list
        for x in self.blufis.keys():
            print(">>>>>> Reset Blufi BLE of device " + str(x) + " ...")
            blufiId_list.append(int(x))
        for i, data in enumerate(blufiId_list):
            blufiId_array = blufiId_array + str(int(data))
            if i != len(blufiId_list) - 1:
                blufiId_array = blufiId_array + ","
        # [Debug] Print-out created list and array
        # print(blufiId_list)
        # print(blufiId_array)
        # Call the REST API (HTTPS, PUT method) to reset these Blufi's BLE components
        r = requests.put(self.host + "/papis/v1/projects/" + str(self.project_id) + "/devices/blufis/_bleReset",
                         headers={'BZID': self.key, 'Accept': 'application/json'}, data=blufiId_array)
        if r.status_code == 200:
            print("+++ Reset Blufi BLE: OK")
        elif r.status_code == 401:
            print("--- Reset Blufi BLE: ERROR -- unauthorized")
        elif r.status_code == 403:
            print("--- Reset Blufi BLE: ERROR -- forbidden")
        elif r.status_code == 404:
            print("--- Reset Blufi BLE: ERROR -- not found")
        elif r.status_code == 480:
            print("--- Reset Blufi BLE: ERROR -- Error \n" + str(r.content))
        elif r.status_code == 500:
            print("--- Reset Blufi BLE: ERROR -- Server Error \n" + str(r.content))
        else:
            print("--->>> HTTPS response: unknown ERROR (" + str(r.status_code) + ")")

    # (B1.3) This REST API updates (a single or  various) Blufi(s) -- dependency Get Blufis needs to be run front-up
    def update_all_blufi(self):
        # [4.6.11] Update Blufi --> PUT /papis/v1/projects/{projectId}/devices/blufis/{deviceId}
        for x in self.blufis.keys():
            print(">>>>>> Update Blufi " + str(x) + " ...")
            r = requests.put(self.host + "/papis/v1/projects/" + str(self.project_id) + "/devices/blufis/" + x,
                             headers={'BZID': self.key, 'Accept': 'application/json'}, data=one_blufi_object)
            # Note: Using blufi_objects (= Get Blufis response) returns Python Exit Code 1 + error message:
            #       "ValueError: too many values to unpack (expected 2)".
            #       Set HTTPS body as "one_blufi_object" ==> Python Exit Code 0, but: 5x ERROR CODE 500
            # (Use the Get Blufis response as Update Blufi request body --> advised by Bluvision support)
            if r.status_code == 200:
                print("+++ Update Blufi: OK")
                r = r.json()
                print("--->>> Blufi Data returned: \n" + r)
            elif r.status_code == 401:
                print("--- Update Blufi: ERROR -- unauthorized")
            elif r.status_code == 403:
                print("--- Update Blufi: ERROR -- forbidden")
            elif r.status_code == 404:
                print("--- Update Blufi: ERROR -- not found")
            elif r.status_code == 480:
                print("--- Update Blufi: ERROR -- Error \n" + str(r.content))
            elif r.status_code == 500:
                print("--- Update Blufi: ERROR -- Server Error")
                # print("--- Update Blufi: ERROR -- Server Error \n" + str(r.content))
            else:
                print("--->>> HTTPS response: unknown ERROR (" + str(r.status_code) + ")")

    # (B1.4) This REST API deletes (all specified) Blufi(s)
    # Note: this method is untested, just implemented --> critical system operation !!!
    def delete_all_blufi(self):
        # [4.6.3] Delete Blufi --> PUT /papis/v1/projects/{projectId}/devices/blufis/_delete
        blufiId_deleteList = []     # Create a BlufiId delete-list (IDs == string !)
        blufiId_deleteArray = ""    # Create a BlufiId delete-array without [ and ] (IDs == str with ' and ')
        # Loop over all Blufis available in order to create a Blufi ID list
        for x in self.blufis.keys():
            print(">>>>>> Delete Blufi " + str(x) + " ...")
            blufiId_deleteList.append(x)
        for i, data in enumerate(blufiId_deleteList):
            blufiId_deleteArray = blufiId_deleteArray + "'" + str(data) + "'"
            if i != len(blufiId_deleteList) - 1:
                blufiId_deleteArray = blufiId_deleteArray + ","
        # [Debug] Print-out created list and array
        # print(blufiId_deleteList)
        # print(blufiId_deleteArray)
        # Call the REST API (HTTPS, PUT method) to reset these Blufi's BLE components
        r = requests.put(self.host + "/papis/v1/projects/" + str(self.project_id) + "/devices/blufis/_delete",
                         headers={'BZID': self.key, 'Accept': 'application/json'}, data=blufiId_deleteArray)
        if r.status_code == 204:
            print("--- Delete Beacon: ERROR -- no content")
        elif r.status_code == 401:
            print("--- Delete Beacon: ERROR -- unauthorized")
        elif r.status_code == 403:
            print("--- Delete Beacon: ERROR -- forbidden")
        elif r.status_code == 404:
            print("--- Delete Beacon: ERROR -- not found")
        elif r.status_code == 480:
            print("--- Delete Beacon: ERROR -- Error \n" + r.content)
        elif r.status_code == 500:
            print("--- Delete Beacon: ERROR -- Server Error \n" + str(r.content))
        else:
            print("--->>> HTTPS response: unknown ERROR or OK (" + str(r.status_code) + ")")

    # (B1.5) This REST API returns a list of all policies that the given Blufi belongs to.
    # Note: A policy defines a set of rules that is checked against the Blufi and its data.
    #       If those rules are violated, Bluzone will create a policy violation and a policy event.
    def get_blufi_policies(self):
        # [4.6.15] Get Blufi Policies --> GET /papis/v1/projects/{projectId}/devices/blufis/{deviceId}/policies
        for x in self.blufis.keys():
            print(">>>>>> Get Blufi " + str(x) + " Policies ...")
            # Loop over all Blufis available and check their policies
            r = requests.get(self.host + "/papis/v1/projects/" + self.project_id + "/devices/blufis/" + x + "/policies",
                             headers={'BZID': self.key, 'Accept': 'application/json'})
            print("--->>> Blufi Policies:")
            r = r.json()
            if not r:
                print("--- No entries!")
            else:
                print(r)

    # (B1.6) This REST API retrieves currently active policy violations for given Blufi(s)
    def get_blufi_policy_violations(self):
        # [4.6.23] Get Blufi Policy Violations --> GET /papis/v1/projects/{projectId}/devices/blufis/{deviceId}/violations ==> [], ok
        for x in self.blufis.keys():
            print(">>>>>> Get Blufi " + str(x) + " Policy Violation ...")
            # Loop over all Blufis available and check their policy violations
            r = requests.get(self.host + "/papis/v1/projects/" + self.project_id + "/devices/blufis/" + x + "/violations",
                             headers={'BZID': self.key, 'Accept': 'application/json'})
        print("--->>> Blufi Policy Violations:")
        r = r.json()
        if not r:
            print("--- No entries!")
        else:
            print(r)

    # (B1.7) This REST API returns all existing policy events that are associated with given Blufi(s).
    # Note: _query version used (POST instead of GET), as recommended by Bluvision support
    def get_blufi_events_pages(self):
        # [4.6.13] Get Blufi Events paged --> POST /papis/v1/projects/{projectId}/devices/blufis/{deviceId}/events/_query
        body_data = {"pageSize": 1, "pageNumber": 1, "sortAsc": True}   # Example request body, inspired by Bluvision support ...
        for x in self.blufis.keys():
            print(">>>>>> Get Blufi " + str(x) + " Policy Events ...")
            # Loop over all Blufis available and check their events
            r = requests.post(self.host + "/papis/v1/projects/" + self.project_id + "/devices/blufis/" + x + "/events/_query",
                              headers={'BZID': self.key, 'Accept': 'application/json'}, json=body_data)
            if r.status_code == 200:
                print("+++ Blufi get Events: OK")
                r = r.json()
                print("--->>> Blufi Data returned: \n" + str(r))
            elif r.status_code == 401:
                print("--- Blufi get Events: ERROR -- unauthorized")
            elif r.status_code == 403:
                print("--- Blufi get Events: ERROR -- forbidden")
            elif r.status_code == 404:
                print("--- Blufi get Events: ERROR -- not found")
            elif r.status_code == 480:
                print("--- Blufi get Events: ERROR -- Error \n" + str(r.content))
            elif r.status_code == 500:
                print("--- Blufi get Events: ERROR -- Server Error \n" + str(r.content))
            else:
                print("--->>> HTTPS response: unknown ERROR (" + str(r.status_code) + ")")

    # 4.6.14 Get Blufi Location --> GET /papis/v1/projects/{projectId}/devices/blufis/{deviceId}/location
    # --> not implemented
    # Description:
    # ------------
    # When provisioning a device in Bluzone it gets associated with a static location.
    # This location defines the area (i.e. geocoordinate bounds) in which a project’s devices are placed.
    # A project can have multiple locations. This API retrieves the location object that the given Blufi belongs to.
    # The relation between these static locations and the location data stream (WSS) is what the stream will provide you
    # with the current positions of your beacons within those beacon’s static location (i.e. area).

    # 4.6.7 Get Blufi Scan Data --> GET /papis/v1/projects/{projectId}/devices/blufis/{blufiId}/scanData
    # --> not implemented
    # Description:
    # ------------
    # Blufi scan data contains overview information about beacons that are currently in range of the given Blufi
    # (e.g.: last known beacon signal strength, temperature). This can be useful in order to perform a quick check
    # on the status of a Blufi and nearby beacons. The GET Blufi response body will also return this information as
    # a part of the returned Blufi object.
    # The difference to the scan data streams (WSS) is that this APIs scan data object will only provide you with a
    # snapshot of device status whereas the stream data will continuously provide all received scan data.

    # (B) CONFIGURE / RECONFIGURE DEVICES -- part (II) BEEKS / Beacons ["beaconId" mostly needed]

    # (B2.1) This REST API gets all Beacon objects and related Beacon data
    def get_beacons(self):
        # [4.4.1] Get Beacons --> GET /papis/v1/projects/{projectId}/devices/beacons
        r = requests.get(self.host + "/papis/v1/projects/" + self.project_id + "/devices/beacons",
                         headers={'BZID': self.key, 'Accept': 'application/json'})
        r = r.json()
        self.beacons = dict()
        global beacon_objects
        for x in r:
            if "deviceId" in x:
                self.beacons[x["deviceId"]] = Devices.Beacon(x)
                beacon_objects.append(x)
            # [Debug] print(beacon_objects)

    # 4.4.3 Update Beacon Configuration --> PUT /papis/v1/projects/{projectId}/devices/beacons/_config
    # --> can be used, but only this way: query and adjust existing configurations!
    # Requirements: provide a list of beacon device IDs in its body as well as a beacon configuration object
    # in its “config” field. The beacon configuration shall be retrieved by using the Get Beacon API and
    # re-using the returned beacon object’s “configuration” field.
    # User for multiple Beacons: better to use "Update Beacon Configuration by Template [4.4.4]
    # --> Not implemented now (both)

    # 4.4.2 Update Beacon Bulk Configuration --> PUT /papis/v1/projects/{projectId}/devices/beacons/_bulkConfig
    # --> Recommended to not use, almost deprecated! Will never be implemented

    # (B2.2) This REST API updates (a single or  various) Beacon(s) -- dependency: Get Beacons (see above)
    def update_all_beacon(self):
        # [4.4.9] Update Beacon --> PUT /papis/v1/projects/{projectId}/devices/beacons/{beaconId}
        for x in self.beacons.keys():
            print(">>>>>> Update Beacon " + str(x) + " ...")
            r = requests.put(self.host + "/papis/v1/projects/" + str(self.project_id) + "/devices/beacons/" + x,
                             headers={'BZID': self.key, 'Accept': 'application/json'}, data=one_beacon_object)
            # Note: Using beacon_objects (= Get Blufis response) returns Python Exit Code 1 + error message:
            #       "ValueError: too many values to unpack (expected 2)".
            #       Set HTTPS body as "one_beacon_object" ==> Python Exit Code 0, but: 12x ERROR CODE 500
            # Bluvision support hint:
            # -----------------------
            # It is expected that you first use the GET Beacon APIs (4.4.1) in order to retrieve an existing beacon
            # object. You can then make your adjustments to that object and send it back using this API.
            if r.status_code == 200:
                print("+++ Update Beacon: OK")
                r = r.json()
                print("--->>> Beacon Data returned: \n" + r)
            elif r.status_code == 401:
                print("--- Update Beacon: ERROR -- unauthorized")
            elif r.status_code == 403:
                print("--- Update Beacon: ERROR -- forbidden")
            elif r.status_code == 404:
                print("--- Update Beacon: ERROR -- not found")
            elif r.status_code == 480:
                print("--- Update Beacon: ERROR -- Error \n" + str(r.content))
            elif r.status_code == 500:
                print("--- Update Beacon: ERROR -- Server Error")
                # print("--- Update Beacon: ERROR -- Server Error \n" + str(r.content))
            else:
                print("--->>> HTTPS response: unknown ERROR (" + str(r.status_code) + ")")

    # (B2.3) This REST API deletes (all specified) Beacon(s)
    # Note: this method is untested, just implemented --> critical system operation !!!
    def delete_all_beacon(self):
        # [4.4.5] Delete Beacons --> PUT /papis/v1/projects/{projectId}/devices/beacons/_delete
        for x in self.beacons.keys():
            print(">>>>>> Delete Beacon of device " + str(x) + " ...")
            # Loop over all Beacons available and delete all of them
            r = requests.put(self.host + "/papis/v1/projects/" + str(self.project_id) + "/devices/beacons/_delete",
                             headers={'BZID': self.key, 'Accept': 'application/json'},
                             json={'beaconIds': x})
            # Note: probably would need to be data=... instead of json=... including an array of Beacon IDs to delete
            #       (see above, implementation of: delete_all_blufi).
            if r.status_code == 204:
                print("--- Delete Beacon: ERROR -- no content")
            elif r.status_code == 401:
                print("--- Delete Beacon: ERROR -- unauthorized")
            elif r.status_code == 403:
                print("--- Delete Beacon: ERROR -- forbidden")
            elif r.status_code == 404:
                print("--- Delete Beacon: ERROR -- not found")
            elif r.status_code == 480:
                print("--- Delete Beacon: ERROR -- Error \n" + str(r.content))
            elif r.status_code == 500:
                print("--- Delete Beacon: ERROR -- Server Error \n" + str(r.content))
            else:
                print("--->>> HTTPS response: unknown ERROR or OK (" + str(r.status_code) + ")")

    # (B2.4) This REST API returns a list of all policies that the given Beacon belongs to.
    # Note: equivalent of the Blufi policies query.
    def get_beacon_policies(self):
        # [4.4.14] Get Beacon Policies --> GET /papis/v1/projects/{projectId}/devices/beacons/{beaconId}/policies
        for x in self.beacons.keys():
            print(">>>>>> Get Beacon " + str(x) + " Policies ...")
            # Loop over all Beacons available and check their policies
            r = requests.get(self.host + "/papis/v1/projects/" + self.project_id + "/devices/beacons/" + x + "/policies",
                             headers={'BZID': self.key, 'Accept': 'application/json'})
            # print("--->>> Beacon Policies:")
            r = r.json()
            if not r:
                print("--->>> Beacon Policies: no entries!")
            else:
                # [Lots of data ... skip printing ...] print(r)
                print("--->>> Beacon Policies: available.")

    # (B2.5) This REST API retrieves currently active policy violations for given Beacon(s)
    def get_beacon_policy_violations(self):
        # [4.4.26] Get Beacon Policy Violations --> GET /papis/v1/projects/{projectId}/devices/beacons/{beaconId}/violations
        for x in self.beacons.keys():
            print(">>>>>> Get Beacon " + str(x) + " Policy Violation ...")
            # Loop over all Beacons available and check their policy violations
            r = requests.get(self.host + "/papis/v1/projects/" + self.project_id + "/devices/beacons/" + x + "/violations",
                             headers={'BZID': self.key, 'Accept': 'application/json'})
        print("--->>> Beacon Policy Violations:")
        r = r.json()
        if not r:
            print("--- No entries!")
        else:
            print(r)

    # (B2.6) This REST API returns all existing policy events that are associated with given Beacon(s).
    # Note: _query version used (POST instead of GET), as recommended by Bluvision support
    def get_beacon_events_pages(self):
        # [4.4.12] Get Beacon Events paged --> POST /papis/v1/projects/{projectId}/devices/beacons/{beaconId}/events/_query
        body_data = {"pageSize": 1, "pageNumber": 1, "sortAsc": True}  # Example request body, inspired by Bluvision support ...
        for x in self.beacons.keys():
            print(">>>>>> Get Beacon " + str(x) + " Policy Events ...")
            # Loop over all Beacons available and check their events
            r = requests.post(self.host + "/papis/v1/projects/" + self.project_id + "/devices/blufis/" + x + "/events/_query",
                              headers={'BZID': self.key, 'Accept': 'application/json'}, json=body_data)
            # Result: 7x "Server Error" + 5x "OK" -- clear why ???
            if r.status_code == 200:
                print("+++ Beacon get Events: OK")
                r = r.json()
                print("--->>> Beacon Data returned: \n" + str(r))
            elif r.status_code == 401:
                print("--- Beacon get Events: ERROR -- unauthorized")
            elif r.status_code == 403:
                print("--- Beacon get Events: ERROR -- forbidden")
            elif r.status_code == 404:
                print("--- Beacon get Events: ERROR -- not found")
            elif r.status_code == 480:
                print("--- Beacon get Events: ERROR -- Error \n" + str(r.content))
            elif r.status_code == 500:
                print("--- Beacon get Events: ERROR -- Server Error")
                # print("--- Beacon get Events: ERROR -- Server Error \n" + str(r.content))
            else:
                print("--->>> HTTPS response: unknown ERROR (" + str(r.status_code) + ")")

    # 4.4.15 Get Beacon Scan Data --> GET /papis/v1/projects/{projectId}/devices/beacons/{beaconId}/scanData
    # --> not implemented
    # Description (by  Bluvision support):
    # ------------------------------------
    # This is the inverted beacon status snapshot API (see above: 4.6.7).
    # Instead of all beacons that are seen by a specific Blufi, this API will provide a snapshot of which Blufi
    # devices are currently seeing the specified beacon. Besides (and again):
    # the difference to the scan data streams (WSS) is that this APIs scan data object will only provide you with a
    # snapshot of device status whereas the stream data will continuously provide all received scan data.

    # --- Further beacon / device RESTful calls below ...

    def get_beacon_jobs(self):
        # [Chapter 4.11.6] GET /papis/v1/projects/{projectId}/devices/beacons/{beaconId}/jobs
        job_list = list()
        for x in self.beacons.keys():
            r = requests.get(self.host + "/papis/v1/projects/" + self.project_id + "/devices/beacons/" + x + "/jobs",
                             headers={'BZID': self.key, 'Accept': 'application/json'})
            r = r.json()
            for i in r:
                if "deviceJobId" in i:
                    job_list.append(i)
        print(">>>>>> Beacon Jobs:")
        # ToDo: lots of response data! Thus parse for relevant / important data, e.g. "deviceJobId" or "deviceJobScheduleId". Only print this.
        print(job_list)

    def get_device_registry(self):
        # [Chapter 4.12.1] GET /papis/v1/projects/{projectId}/registry/devices
        r = requests.get(self.host + "/papis/v1/projects/" + self.project_id + "/registry/devices",
                         headers={'BZID': self.key, 'Accept': 'application/json'})
        r = r.json()
        print(">>>>>> Registered Devices:")
        # ToDo: lots of response data! Thus parse for relevant / important data, e.g. "deviceId". Only print this.
        print(r)

    # Print the device info -- for test and debug
    def print_devices(self):
        print(">>> [Account] --> Print all decives.")
        # Print BluFis ...
        print(">>> BluFis: ")
        for x in self.blufis.values():
            print(x.info)
        # Print Beacons ...
        print(">>> Beacons: ")
        for x in self.beacons.values():
            print(x.info)

    def print_devices_ids(self):
        print(">>> [Account] --> Print all decive's ids.")
        # Print BluFis ...
        print(">>> BluFis: ")
        for x in self.blufis.keys():
            print(x)
        # Print Beacons ...
        print(">>> Beacons: ")
        for x in self.beacons.keys():
            print(x)

    def update_beacon_data(self, beaconId, data):
        if beaconId in self.beacons:
            self.beacons[beaconId].update_data(data)
            print(">>> [Account]: Beacon Updated")
        else:
            print("Beacon not found")

    def get_all_location_data(self):
        print(">>> [Account]: HTTPS --> Request --> GET: (BluFi) locations.")
        # [Chapter 4.17.2] -- Get locations -- GET /papis/v1/projects/{projectId}/locations
        r = requests.get("https://bluzone.io/portal/papis/v1/projects/" + self.project_id + "/locations",
                         headers={'BZID': self.key, 'Accept': 'application/json'})
        r = r.json()
        # Fill the locations list and extract locationId
        for x in r:
            self.locations.append(Location.Location(x))
            self.location_id = x["locationId"]
        # Print-out "(BluFi) locations"
        print(">>> (BluFi) locations >>>")
        print("")
        for x in self.locations:
            print(x.info)
            print("")
        # If locationId is not part of the JSON response, finish here
        if self.location_id is None:
            return
        # Otherwise continue

        print(">>> [Account]: HTTPS --> Request --> GET: location.")
        # [Chapter 4.17.5] -- Get location -- GET /papis/v1/projects/{projectId}/locations/{locationId}
        r = requests.get(
            "https://bluzone.io/portal/papis/v1/projects/" + self.project_id + "/locations/" + str(self.location_id),
            headers={'BZID': self.key, 'Accept': 'application/json'})
        r = r.json()
        # Print-out "(BluFi) location"
        print(">>> (BluFi) location >>>")
        print("")
        print(r)
        print("")

        # GET LOCATIONS and GET LOCATION delivers exactly the same data !!

        print(">>> [Account]: HTTPS --> Request --> GET: Beacons in location.")
        # [Chapter 4.17.7] -- Get Beacons in location -- /papis/v1/projects/{projectId}/locations/{locationId}/beacons
        r = requests.get("https://bluzone.io/portal/papis/v1/projects/" + self.project_id + "/locations/" + str(
            self.location_id) + "/beacons", headers={'BZID': self.key, 'Accept': 'application/json'})
        r = r.json()
        for x in r:
            self.beacon_locations.append(Location.Location(x))
        # Print-out "Beacons in location"
        print(">>> Beacons in location >>>")
        print("")
        for x in self.beacon_locations:
            print(x.info)
            print("")

        # Lots of data !

        print(">>> [Account]: HTTPS --> Request --> GET: Blufis in location.")
        # [Chapter 4.17.8] -- Get Blufis in location -- GET /papis/v1/projects/{projectId}/locations/{locationId}/blufis
        r = requests.get("https://bluzone.io/portal/papis/v1/projects/" + self.project_id + "/locations/" + str(
            self.location_id) + "/blufis", headers={'BZID': self.key, 'Accept': 'application/json'})
        r = r.json()
        # Print-out "(Blufis in location"
        print(">>> Blufis in location >>>")
        print("")
        print(r)
        print("")

        print(">>> [Account]: HTTPS --> Request --> GET: location data.")
        # [Chapter 4.17.9] -- Get location data -- GET /papis/v1/projects/{projectId}/locations/{locationId}/data
        r = requests.get("https://bluzone.io/portal/papis/v1/projects/" + self.project_id + "/locations/" + str(
            self.location_id) + "/data", headers={'BZID': self.key, 'Accept': 'application/json'})
        r = r.json()
        for x in r:
            self.location_data.append(Location.Location(x))
        # Print-out "Location data"
        print(">>> Location data >>>")
        print("")
        for x in self.location_data:
            print(x.info)
            print("")

            # Location data has (obviously) 8 entries, but all empty !?

    def do_provisioning(self):
        # [4.21.2] Register Beacon POST /sapis/v1/provisioning/beacons ==> ERROR CODE 500
        r = requests.post("https://bluzone.io/portal/sapis/v1/provisioning/beacons",
                          headers={'BZID': self.key, 'Accept': 'application/json'},
                          data=self.beacons[0].info["deviceId"])
        r = r.json()
        print(">>> Register Beacon with device ID ", self.beacons[0].info["deviceId"])
        print("")
        print(r)
        print("")

        # [4.21.3] Get Beacon Signal POST /sapis/v1/provisioning/beacons/signal ==> ERROR CODE 500
        r = requests.post("https://bluzone.io/portal/sapis/v1/provisioning/beacons/signal",
                          headers={'BZID': self.key, 'Accept': 'application/json'},
                          data=self.beacons[0].info["id64"])
        r = r.json()
        print(">>> Get signal of provisioned device (Beacon) with ID ", self.beacons[0].info["deviceId"])
        print("")
        print(r)
        print("")

        # [4.21.4] Get Provisioning Beacon GET /sapis/v1/provisioning/beacons/{beaconId} ==> OK (200)
        r = requests.get("https://bluzone.io/portal/sapis/v1/provisioning/beacons/" + self.beacons[0].info["deviceId"],
                         headers={'BZID': self.key, 'Accept': 'application/json'})
        r = r.json()
        print(">>> Get provisioning of Beacon with device ID ", self.beacons[0].info["deviceId"])
        print("")
        print(r)
        print("")

        # [4.21.9] Get Beacon Templates GET /sapis/v1/provisioning/beacons/{deviceId}/templates
        r = requests.get("https://bluzone.io/portal/sapis/v1/provisioning/beacons/" + self.beacons[0].info[
            "deviceId"] + "/templates", headers={'BZID': self.key, 'Accept': 'application/json'})
        r = r.json()
        print(">>> Get template (and templateId) of Beacon with device ID ", self.beacons[0].info["deviceId"])
        print("")
        print(r)
        print("")

    # (C) PERFORM DEVICE PROVISIONING / REPROVISIONING --> NOT POSSIBLE !!!
    # ------------------------------------------------

    # --> Genearal Bluvision support team information:
    #     --------------------------------------------
    # These APIs are used by the Bluzone mobile app in order to register and provision new devices.
    # During the provisioning process the app will establish a BLE connection to the given device in order to
    # configure it based on the data received from Bluzone via the below APIs.
    # This is a highly complex process that is only used internally.
    # We currently don’t support implementations of custom apps around this !!!

    # --> Some support response details:
    #     ------------------------------

    # 4.21.2 Register Beacon --> POST /sapis/v1/provisioning/beacons
    # This API is used to add new beacons to an existing Bluzone project.
    # You can use the response to the GET Beacon API as a template containing the fields that are required for this API.
    # Fields like the beacon’s device type and firmware information can be omitted as those are auto-filled during
    # the registration process.

    # 4.21.3 Get Beacon Signal --> POST /sapis/v1/provisioning/beacons/signal
    # This API will only return a valid response in case the given beacon is seen by nearby connected Blufis.
    # The request body should be a list of beacon device ids as strings (e.g.: [“<beaconId1>“, “<beaconId2>”, …,
    # “<beaconIdN>”]). The response will contain each beacon’s current signal level to nearby Blufis.
    # This can be used as an indicator for the quality of the beacon’s reception.

    # (D.1) This REST API returns a list of all existing policies.
    def get_policies(self):
        # [4.19.2] Get Policies --> GET /papis/v1/projects/{projectId}/policies
        r = requests.get(self.host + "/papis/v1/projects/" + self.project_id + "/policies",
                         headers={'BZID': self.key, 'Accept': 'application/json'})
        r = r.json()
        global policy_objects
        policy_objects = r
        print("--->>> Get policies:")
        # ToDo: lots of response data! Thus parse for relevant / important data, e.g. "policyId" or "locationId".
        # [Debug] print(policy_objects)

    # (D.2) This REST API updates a single (existing!) policy.
    def update_one_policy(self):
            # [4.19.8] Update Policy --> PUT /papis/v1/projects/{projectId}/policies/{policyId} ==> ERROR CODE 500
            r = requests.put(self.host + "/papis/v1/projects/" + str(self.project_id) + "/policies/" + str(one_policy_id),
                             headers={'BZID': self.key, 'Accept': 'application/json'}, data=one_policy_object)
            # Note: Using policy_objects (= Get Policies response) returns Python Exit Code 1 + error message:
            #       "ValueError: too many values to unpack (expected 2)".
            #       Set HTTPS body as "one_policy_object" ==> Python Exit Code 0, but: Server error (ERROR CODE 500)
            # Description:
            # ------------
            # Updating a policy will only work for existing policies.
            # The flow for updating a policy would then require you to first use the GET Policy APIs (4.19.2)
            # and then adjusting the returned policy object for the PUT Policy API.
            if r.status_code == 200:
                print("+++ Update Policy " + str(one_policy_id) + ": OK")
                r = r.json()
                print("--->>> Policy Data returned: \n" + r)
            elif r.status_code == 401:
                print("--- Update Policy: ERROR -- unauthorized")
            elif r.status_code == 403:
                print("--- Update Policy: ERROR -- forbidden")
            elif r.status_code == 404:
                print("--- Update Policy: ERROR -- not found")
            elif r.status_code == 480:
                print("--- Update Policy: ERROR -- Error \n" + str(r.content))
            elif r.status_code == 500:
                print("--- Update Policy: ERROR -- Server Error \n" + str(r.content))
            else:
                print("--->>> HTTPS response: unknown ERROR (" + str(r.status_code) + ")")

    # (D.3) This REST API deletes a single (existing!) policy.
    # Note: this method is untested, just implemented --> critical system operation !!!
    def delete_one_policy(self):
        # [4.19.3] Delete Policy --> PUT /papis/v1/projects/{projectId}/policies/_delete
        r = requests.put(self.host + "/papis/v1/projects/" + str(self.project_id) + "/policies/_delete",
                         headers={'BZID': self.key, 'Accept': 'application/json'}, data={'policyIds': one_policy_id})
        if r.status_code == 200:
            print("+++ Delete Policy: ERROR -- OK")
        elif r.status_code == 401:
            print("--- Delete Policy: ERROR -- unauthorized")
        elif r.status_code == 403:
            print("--- Delete Policy: ERROR -- forbidden")
        elif r.status_code == 404:
            print("--- Delete Policy: ERROR -- not found")
        elif r.status_code == 480:
            print("--- Delete Policy: ERROR -- Error \n" + str(r.content))
        elif r.status_code == 500:
            print("--- Delete Policy: ERROR -- Server Error \n" + str(r.content))
        else:
            print("--->>> HTTPS response: unknown ERROR or OK (" + str(r.status_code) + ")")

    # (D) CHANGE and ADAPT POLICIES
    # -----------------------------

    # --> Support response details:
    #     -------------------------

    # 4.19.1 Create policy --> POST /papis/v1/projects/{projectId}/policies
    # --> not implemented, and should not be !
    # Description by Bluvision support:
    # ---------------------------------
    # Bluzone supports policies of different types.
    # The parameters used to create a new policy will vary based on the selected policy type.
    # We recommend that you create policies of each kind that you are interested in using the Bluzone Console.
    # You can then use the created policy objects as templates in order to review required settings.

    # 4.19.7 Get Policy --> GET /papis/v1/projects/{projectId}/policies/{policyId}
    # --> not implemented, neither planned !
    # Basically redundant with 4.19.2 (implemented), just for one single policy.

    # 4.19.12 Update Policies for Beacon --> PUT /papis/v1/projects/{projectId}/policies/{policyId}/beacons
    # --> not implemented, perhaps still later.
    # Description by Bluvision support:
    # ---------------------------------
    # The request body for this API is a list of beacon device ids (e.g.: [“<beaconId1>“, “<beaconId2>”, …,
    # “<beaconIdN>”]). This API will set the given policy’s list of associated beacons.
    # All beacons that you specify will be added to the policy. Beacons that have previously been associated
    # and that are not in the request body will be removed from the policy.
    # Requests to this API will be ignored if a policy is set to be globally scoped - i.e. in case the policy’s
    # ‘globalScope’ field is set to ‘true’. In that case all of the project’s beacons are associated with the policy.

    # 4.19.14 Update Policies for Blufi --> PUT /papis/v1/projects/{projectId}/policies/{policyId}/blufis
    # --> not implemented, perhaps still later.
    # This is equivalent to 4.19.12 and can be used to set the list of Blufi devices associated with a policy.

    # 4.19.15 Get Policy Events --> GET /papis/v1/projects/{projectId}/policies/{policyId}/events
    # --> Redundant to what we have, separate for Beacons and Blufis. Hence no goal implementing it !
    # Description by Bluvision support:
    # ---------------------------------
    # A policy event is created each time when a device (Beacon or Blufi) that is associated with a policy violates
    # the rules set by that policy. The event will be generated with a status of ‘violating’.
    # Once the same device starts conforming to the policy’s rules again the event will move into an ‘ok’ state
    # and will thereby be concluded.
    # In summary, a policy event describes the details and lifecycle of a policy violation.
    # In addition to a policy event, Bluzone will create a policy violation each time that a policy is violated.
    # The difference here is that policy events are persistent and can be used to view a policy’s violation history
    # whereas policy violations only live while the policy is being violated and are deleted from the system once
    # the violation has passed.

    # 4.19.18 Get Policy Violations for Policy --> GET /papis/v1/projects/{projectId}/policies/{policyId}/violations
    # --> Could be useful - potentially implement later
    # Description by Bluvision support:
    # ---------------------------------
    # This API will return all currently active violations of the given policy (i.e. multiple devices, one policy)
    # whereas the related Blufi / Beacon APIs return violations for the given device (i.e. one device, multiple policies).

    # 4.19.19 Clear Policy Violations --> DELETE /papis/v1/projects/{projectId}/policies/{policyId}/violations
    # --> May be useful (but since system clears automatically... ?!), thus not implemented (+ violation list == []
    # Is obviously supposed to clear a violation manually.

    # (E) CONFIGURE / RECONFIGURE the WSS STREAM Interface ==> NEITHER POSSIBLE NOR NEEDED !!!
    #     --> [4.23.x]: used to export stream data into an own Amazon Kinesis stream setting more params.

# Nothing directly executed --> the below is called from module "bluzone_https_com"!
#   acc = Account("eezVpHmdcf5K24Lx7rR3wGIDeDNjPHDTYM7i3GgIMTE8rAEAa3", "3267", "bluzone@satusmed.com.br", "Bluzone123!")
