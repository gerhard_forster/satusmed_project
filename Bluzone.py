import Account
import Stream
import json
from Packet import ScanData, Location, Policy
import threading
import time
from Log import Log


# Class that encapsulates all features
class Bluzone:
    # Constructor
    def __init__(self):
        self.__account = None
        self.__stream = {}
        self.__stream_thread = {}
        self.__error_log = []

    # Functions to create objects. Idea is to hide Account and Stream objects
    #############################################
    def add_stream(self, host, key, stype):
        self.__stream[stype] = Stream.Stream(host, key)

    def add_account(self, key, project_id, usr, ps):
        self.__account = Account.Account(key, project_id, usr, ps)

    ##############################################

    # Error Calls
    ##############################################
    def errorlog(self):
        if self.__error_log:
            f = open("Errors/log" + self.space2underline(time.asctime(time.localtime(time.time()))) + ".txt", "w")
            for x in self.__error_log:
                f.write(x.toString() + "\n")
            f.close()

    @staticmethod
    def space2underline(s):
        res = ""
        for x in s:
            if x == ' ' or x == ':':
                res += '_'
            else:
                res += x
        return res

    # Stream calls
    ##############################################
    def connect_stream(self, stype):
        if self.__stream is not None:
            self.__stream[stype].connect()

    def run_stream(self, stype):
        if self.__stream[stype] is not None:
            print(">>> Stream Started")
            self.__stream_thread[stype] = threading.Thread(target=self.__stream[stype].run, args=(self.__on_message,))
            self.__stream_thread[stype].start()

    def close_stream(self, stype):
        if self.__stream_thread[stype] is not None:
            self.__stream[stype].close_stream()
            self.__stream_thread[stype].join()

    def __on_message(self, ws, message):
        try:
            print(">>> [Stream] --> Message >>> ")
            message = json.loads(message)
            if "mac" in message:
                data = Location(message)
            elif "id" in message:
                data = ScanData(message)
                self.__account.update_beacon_data(message["id"], message)
            elif "policyId" in message:
                data = Policy(message)
            print(message)
        except Exception as wse:
            self.__error_log.append(Log(wse))

    ##############################################

    # Account calls
    ##############################################
    def get_devices(self):
        if self.__account is not None:
            self.__account.get_devices()

    def get_device_registry(self):
        if self.__account is not None:
            self.__account.get_device_registry()

    def print_devices(self):
        if self.__account is not None:
            self.__account.print_devices()

    def print_devices_ids(self):
        if self.__account is not None:
            self.__account.print_devices_ids()

    # Beacon part

    def get_beacons(self):
        if self.__account is not None:
            self.__account.get_beacons()

    def update_all_beacon(self):
        if self.__account is not None:
            self.__account.update_all_beacon()

    def delete_all_beacon(self):
        if self.__account is not None:
            self.__account.delete_all_beacon()

    def get_beacon_policies(self):
        if self.__account is not None:
            self.__account.get_beacon_policies()

    def get_beacon_policy_violations(self):
        if self.__account is not None:
            self.__account.get_beacon_policy_violations()

    def get_beacon_events_pages(self):
        if self.__account is not None:
            self.__account.get_beacon_events_pages()

    def get_beacon_jobs(self):
        if self.__account is not None:
            self.__account.get_beacon_jobs()

    # Blufi part

    def reset_all_blufi_ble(self):
        if self.__account is not None:
            self.__account.reset_all_blufi_ble()

    def update_all_blufi(self):
        if self.__account is not None:
            self.__account.update_all_blufi()

    def delete_all_blufi(self):
        if self.__account is not None:
            self.__account.delete_all_blufi()

    def get_blufi_policies(self):
        if self.__account is not None:
            self.__account.get_blufi_policies()

    def get_blufi_policy_violations(self):
        if self.__account is not None:
            self.__account.get_blufi_policy_violations()

    def get_blufi_events_pages(self):
        if self.__account is not None:
            self.__account.get_blufi_events_pages()

    # Policy part

    def get_policies(self):
        if self.__account is not None:
            self.__account.get_policies()

    def update_one_policy(self):
        if self.__account is not None:
            self.__account.update_one_policy()

    def delete_one_policy(self):
        if self.__account is not None:
            self.__account.delete_one_policy()

    ############################################


b = Bluzone()

b.add_stream("wss://bluzone.io/portal/consumer/policy", "eezVpHmdcf5K24Lx7rR3wGIDeDNjPHDTYM7i3GgIMTE8rAEAa3", "policy")
b.add_stream("wss://bluzone.io/portal/consumer/raw", "eezVpHmdcf5K24Lx7rR3wGIDeDNjPHDTYM7i3GgIMTE8rAEAa3", "scanData")
b.add_stream("wss://bluzone.io/portal/consumer/location", "eezVpHmdcf5K24Lx7rR3wGIDeDNjPHDTYM7i3GgIMTE8rAEAa3",
             "location")

b.add_account("eezVpHmdcf5K24Lx7rR3wGIDeDNjPHDTYM7i3GgIMTE8rAEAa3", "3267", "bluzone@satusmed.com.br", "Bluzone123!")

b.connect_stream("policy")
b.connect_stream("scanData")
b.connect_stream("location")

b.get_devices()
b.print_devices_ids()

# b.get_device_registry()

b.reset_all_blufi_ble()
b.update_all_blufi()
# b.delete_all_blufi() -- ToDo: test later !
b.get_blufi_policies()
b.get_blufi_policy_violations()
b.get_blufi_events_pages()

# b.get_beacon_jobs()
b.update_all_beacon()
# b.delete_all_beacon() -- ToDo: test later !
b.get_beacon_policies()
b.get_beacon_policy_violations()
b.get_beacon_events_pages()

b.get_policies()
b.update_one_policy()
# b.delete_one_policy() -- ToDo: test later !

b.run_stream("policy")
b.run_stream("scanData")
b.run_stream("location")

time.sleep(1)

b.close_stream("policy")
b.close_stream("scanData")
b.close_stream("location")

b.errorlog()
