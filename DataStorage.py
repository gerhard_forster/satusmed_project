import time

import os
import os.path


class DataStorage:
    def __init__(self, dataList=None):
        if dataList is None:
            dataList = []
        print(">>> DataStorage constructor")
        self.date = time.asctime(time.localtime(time.time()))
        self.filename = "Datalog/report.txt"
        # Load list elements
        self.data_log = []
        for x in dataList:
            self.data_log.append(x)
            # [Debug] print (self.data_log)

    def dateToString(self):
        return str(self.date)

    # Below: not used
    @staticmethod
    def getFilesOfWorkingDir():
        cwd = os.getcwd()
        files = os.listdir(cwd)
        print("Working directory: '%s' files inside: %s" % (cwd, files))

    # Below: not used
    @staticmethod
    def getScriptPath():
        scriptpath = os.path.dirname(__file__)
        print("The script's exec directory is %s" % scriptpath)

    def headlineLog(self):
        # filename = "Datalog/report.txt"
        f = open(self.filename, "w")  # Open file for data logging
        f.write("=======================================" + "\n")  # Write File trailer (date + headline)
        f.write("[Timestamp] : " + self.dateToString() + "\n")
        f.write("--- Report File ---")
        f.write("\n" + "=======================================" + "\n\n")
        f.close()  # Close data log

    def logDataToFile(self):
        f = open(self.filename, "a")
        if self.data_log:
            for x in self.data_log:
                f.write(x + "\n")
                # Separator of two FSM instances (each representing a badge user!)
                if x == "SYSTEM STATUS: Stop":
                    f.write("\n" + "=======================================" + "\n\n")
            f.close()
            print(" Status: trace data written.")
        else:
            print(" Status: no trace data available.")

    def dataLogger(self):
        # [Debug] self.getFilesOfWorkingDir()
        # [Debug] self.getScriptPath()
        print(">>> DataStorage : data logger")
        # --- Create headline with data log (file open, write and close)
        self.headlineLog()
        # --- Write all log data (re-open, append and close after File write)
        self.logDataToFile()
