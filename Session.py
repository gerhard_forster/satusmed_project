import requests
# Will be used later: import json
from Template import Template


# CLASS Session
class Session:

    # Session construtor
    def __init__(self, username, password):
        print(">>> [Session]: Constructor.")
        self.username = username
        self.password = password
        self.authToken = None
        self.blufi_templates = None
        self.beacon_templates = []

    # Initiate session, store token and authenticate
    def start_session(self):
        print(">>> [Session]: HTTPS --> Request --> POST: User and Password to Bluzone, store Session Token received.")
        r = requests.post("https://bluzone.io/portal/auth/slogin", headers={'Accept': 'application/json', 'Content-Type': 'application/json'}, json={'username': self.username, 'password': self.password})
        r = r.json()
        self.authToken = r["authToken"]

    # Make a request receiving the created BluFi templates
    def get_blufi_templates(self):
        print("[Session -- get_blufi_templates]")
        if self.authToken is None:
            print(">>> [Session] -- ERROR: No authentication token available!")
            return
        print(">>> [Session]: HTTPS --> Request --> GET: BluFi Templates using Session Token.")
        r = requests.get("https://bluzone.io/portal/sapis/v1/provisioning/blufiTemplates", headers={'Accept': 'application/json', 'Cookie': 'X-BZ-AuthToken='+self.authToken})
        r = r.json()
        self.blufi_templates = []
        for x in r:
            self.blufi_templates.append(Template(x))

    @staticmethod
    def get_beacon_templates():
        print("[Session -- get_beacon_templates]")
        # ToDo: implement HTTPS GET method call and array filling later!

# Nothing directly executed.
